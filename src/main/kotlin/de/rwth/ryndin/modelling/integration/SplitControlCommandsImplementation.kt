/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.integration

import de.rwth.ryndin.modelling.component.integration.SplitControlCommands

class SplitControlCommandsImplementation : SplitControlCommands() {
    override fun execute() {
        val commands = controlCommands ?: return
        val actuation = commands.actuation;
        setBrakes(actuation.brakes)
        setEngine(actuation.engine)
        setSteering(actuation.steering)
    }
}
