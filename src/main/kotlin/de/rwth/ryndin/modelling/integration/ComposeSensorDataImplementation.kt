/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.integration

import de.rwth.ryndin.modelling.component.integration.ComposeSensorData
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.SensorData

class ComposeSensorDataImplementation : ComposeSensorData() {
    override fun execute() {
        val v = currentVelocity ?: return
        val c = compass ?: return
        val x = positionX ?: return
        val y = positionY ?: return
        val p = Position(x = x, y = y)
        setSensorData(SensorData(
                velocity = v,
                yawAngle = c,
                position = p
        ))
    }
}
