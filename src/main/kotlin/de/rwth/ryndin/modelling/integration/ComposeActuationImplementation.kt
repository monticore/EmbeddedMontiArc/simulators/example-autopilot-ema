/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.integration

import de.rwth.ryndin.modelling.component.integration.ComposeActuation
import de.rwth.ryndin.modelling.structure.autopilot.Actuation

class ComposeActuationImplementation : ComposeActuation() {
    override fun execute() {
        val e = engine ?: 0.0
        val s = steering ?: 0.0
        val b = brakes ?: 0.0
        setActuation(Actuation(
                engine = e,
                steering = s,
                brakes = b
        ))
    }
}
