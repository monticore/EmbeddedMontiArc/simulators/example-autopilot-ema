/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.geometry.calculateSignedAngleBetween
import de.rwth.ryndin.modelling.autopilot.geometry.toModel
import de.rwth.ryndin.modelling.component.autopilot.common.SignedAngleBetween

class SignedAngleBetweenImplementation : SignedAngleBetween() {
    override fun execute() {
        val structVector1 = v1 ?: return
        val structVector2 = v2 ?: return
        setAngle(calculateSignedAngleBetween(structVector1.toModel(), structVector2.toModel()))
    }
}
