/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.component.autopilot.planning.DecidePathRecalculation

class DecidePathRecalculationImplementation(
        stayOnTrajectoryThreshold: Double
) : DecidePathRecalculation(stayOnTrajectoryThreshold) {

    override fun execute() {
        setIsRecalculatePath(true)
        val givenPath = path ?: return
        if (givenPath.isEmpty()) {
            return
        }
        val distance = distanceToPath ?: return
        val index = indexOfClosestSegment ?: return
        if (distance > stayOnTrajectoryThreshold) {
            if (previousDistanceToPath != null && previousDistanceToPath!! <= distance) {
                return
            }
        }
        if (index > 0) {
            setTrimmedPath(givenPath.drop(index.toInt()))
        } else {
            setTrimmedPath(givenPath)
        }
        setIsRecalculatePath(false)
    }
}
