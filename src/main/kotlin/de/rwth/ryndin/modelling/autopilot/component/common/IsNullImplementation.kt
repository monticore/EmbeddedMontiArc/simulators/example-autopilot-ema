/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.IsNull

class IsNullImplementation : IsNull<Any?>() {
    override fun execute() {
        setIsNull(value == null)
    }
}
