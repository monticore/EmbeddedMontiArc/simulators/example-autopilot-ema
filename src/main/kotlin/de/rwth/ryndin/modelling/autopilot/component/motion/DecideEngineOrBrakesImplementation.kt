/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.motion

import de.rwth.ryndin.modelling.component.autopilot.motion.DecideEngineOrBrakes

class DecideEngineOrBrakesImplementation : DecideEngineOrBrakes() {

    override fun execute() {
        val err = error ?: Double.NaN
        val control = controlSignal ?: Double.NaN
        setEngine(0.0)
        setBrakes(0.0)
        if (!err.isNaN() && !control.isNaN()) {
            if (err > 0.0) {
                setEngine(control)
            } else {
                setBrakes(control)
            }
        }
    }
}
