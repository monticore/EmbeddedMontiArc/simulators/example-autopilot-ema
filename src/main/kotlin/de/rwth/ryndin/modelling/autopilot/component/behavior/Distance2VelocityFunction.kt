/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.DEFAULT_NUMERIC_COMPARATOR
import de.rwth.ryndin.modelling.autopilot.function.Exponent

internal class Distance2VelocityFunction(
        private val comparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
) : (Double) -> Double {

    companion object {
        val X1 = 1.0 // m
        val V1 = 1.0 // m/s
        val X2 = 5.0 // m
        val V2 = 3.0 // m/s
        val X3 = 20.0 // m
        val V3 = 10.0 // m/s
        val X4 = 50.0 // m
        val V4 = 13.0 // m/s
        private val F12 = Exponent(X1, V1, X2, V2)
        private val F34 = Exponent(X3, V3, X4, V4)
    }

    override fun invoke(distanceToFinish: Double): Double {
        val d = distanceToFinish
        return when {
            comparator.compare(d, X1) < 0 -> 0.0
            comparator.compare(d, X2) < 0 -> F12(d)
            comparator.compare(d, X3) < 0 -> V2
            else -> F34(d)
        }
    }
}
