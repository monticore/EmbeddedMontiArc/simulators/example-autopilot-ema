/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.geometry.distanceTo
import de.rwth.ryndin.modelling.autopilot.geometry.isInsideRectangle
import de.rwth.ryndin.modelling.autopilot.geometry.projectOnto
import de.rwth.ryndin.modelling.component.autopilot.common.FindClosestPathSegment

class FindClosestPathSegmentImplementation : FindClosestPathSegment() {

    override fun execute() {
        val givenPosition = position ?: return
        val givenPath = path ?: return
        when {
            givenPath.isEmpty() -> return
            givenPath.size == 1 -> {
                setIndex(0)
                setDistance(givenPosition.distanceTo(givenPath[0]))
                return
            }
        }
        var closestDistance = Double.POSITIVE_INFINITY
        var index = -1
        var i = 1
        while (i < givenPath.size) {
            val p1 = givenPath[i - 1]
            val p2 = givenPath[i]
            val projection = givenPosition.projectOnto(p1, p2)
            val d = if (projection.isInsideRectangle(p1, p2)) {
                givenPosition.distanceTo(projection)
            } else {
                minOf(
                        givenPosition.distanceTo(p1),
                        givenPosition.distanceTo(p2)
                )
            }
            if (d < closestDistance) {
                closestDistance = d
                index = i - 1
            }
            i++
        }
        if (index >= 0 && closestDistance.isFinite()) {
            setIndex(index.toLong())
            setDistance(closestDistance)
        }
    }
}
