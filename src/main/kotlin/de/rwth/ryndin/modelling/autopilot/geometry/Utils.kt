/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.geometry

import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.VehicleState

internal fun Position.isInsideRectangle(p1: Position, p2: Position): Boolean {
    return (p1.x - x) * (p2.x - x) <= 0.0 && (p1.y - y) * (p2.y - y) <= 0.0
}

internal fun Position.signedDistanceToLine(p1: Position, p2: Position): Double {
    val v12 = Vector.create(p1, p2)
    return -(v12.y * x - v12.x * y + p2.x * p1.y - p1.x * p2.y) / v12.norm
}

internal fun Position.projectOnto(p1: Position, p2: Position): Position {
    val v = Vector.create(p1, this)
    val v12 = Vector.create(p1, p2)
    val k = v.dotProduct(v12) / v12.squareNorm
    return p1.plus(v12.multiply(k))
}

internal fun Position.distanceTo(p: Position): Double {
    return Vector.create(this, p).norm
}

internal val VehicleState.currentDirection: Vector
    get() {
        val angle = sensorData.yawAngle + Math.PI / 2
        return Vector(Math.cos(angle), Math.sin(angle))
    }
