/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.Sum

class SumImplementation : Sum() {
    override fun execute() {
        val term1 = t1 ?: return
        val term2 = t2 ?: return
        if (!term1.isNaN() && !term2.isNaN()) {
            setResult(term1 + term2)
        }
    }
}
