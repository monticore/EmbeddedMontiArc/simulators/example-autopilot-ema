/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.structure.autopilot.Node
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.slf4j.LoggerFactory

class NavigatorImplementation : de.rwth.ryndin.modelling.component.autopilot.planning.Navigator() {

    companion object {
        private val LOG = LoggerFactory.getLogger(NavigatorImplementation::class.java)
    }

    private var nodeInformation = emptyMap<Long, Node>()
    private var edgeInformation = emptyMap<Pair<Long, Long>, Double>()
    private var navigator: Navigator? = null

    override fun execute() {
        val edges = allEdges ?: emptyList()
        if (edges.isNotEmpty()) {
            edgeInformation = edges.map {
                Pair(it.nodeFromId, it.nodeToId) to it.distance
            }.toMap()
        }
        val nodes = allNodes ?: emptyList()
        if (nodes.isNotEmpty()) {
            nodeInformation = nodes.map { it.id to it }.toMap()
        }
        val isMapDataChanged = edges.isNotEmpty() || nodes.isNotEmpty()
        if (isMapDataChanged) {
            onMapDataChange()
        } else {
            checkIfGoalChanged()
        }
        calculatePath()
    }

    private fun onMapDataChange() {
        val goal = when {
            goalNodeId != null -> goalNodeId!!
            navigator != null -> navigator!!.goalNodeId
            else -> return
        }
        createNavigator(getStartNodeId(goal), goal)
    }

    private fun checkIfGoalChanged() {
        val goal = goalNodeId ?: return
        if (navigator == null || goal != navigator!!.goalNodeId) {
            createNavigator(getStartNodeId(goal), goal)
        }
    }

    private fun getStartNodeId(fallback: Long): Long {
        return when {
            startNodeId != null -> startNodeId!!
            else -> fallback
        }
    }

    private fun createNavigator(initialStartNodeId: Long, goalNodeId: Long) {
        navigator = DLiteBasedNavigator(
                edgeInformation,
                initialStartNodeId,
                goalNodeId,
                EuclideanHeuristic(nodeInformation)
        )
    }

    private fun calculatePath() {
        val start = startNodeId ?: return
        val navi = navigator ?: return
        val nodeIds = navi.getShortestPathFrom(start)
        val result = mutableListOf<Position>()
        for (nodeId in nodeIds) {
            val node = nodeInformation[nodeId]
            if (node != null) {
                result.add(node.position)
            } else {
                LOG.error("could not find node with id $nodeId")
                return
            }
        }
        setShortestPath(result)
    }
}
