/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.function.TurnCharacteristicMeasurement

internal data class TurnCharacteristicMeasurementDto(
        var radius: Double = 0.0,
        var steering: Double = 0.0,
        var velocity: Double = 0.0
)

internal fun TurnCharacteristicMeasurementDto.toTurnCharacteristicMeasurement(): TurnCharacteristicMeasurement {
    return TurnCharacteristicMeasurement(radius, steering, velocity)
}
