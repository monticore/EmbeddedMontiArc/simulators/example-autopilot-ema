/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.component.autopilot.planning.ChoosePath
import org.slf4j.LoggerFactory

class ChoosePathImplementation : ChoosePath() {

    companion object {
        private val LOG = LoggerFactory.getLogger(ChoosePathImplementation::class.java)
    }

    override fun execute() {
        when {
            recalculatedPath != null -> {
                setResultingPath(recalculatedPath!!)
                LOG.info("planned route $recalculatedPath")
                setIsPathRebuilt(true)
            }
            path != null -> {
                setResultingPath(path!!)
                LOG.info("planned route $path")
                setIsPathRebuilt(false)
            }
        }
    }
}
