/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import com.google.common.graph.NetworkBuilder
import de.rwth.ryndin.modelling.autopilot.DEFAULT_NUMERIC_COMPARATOR
import java.util.LinkedList
import java.util.PriorityQueue

internal class DLiteBasedNavigator(
        initialEdges: Map<Pair<Id, Id>, Double>,
        initialStartNodeId: Id,
        override val goalNodeId: Id,
        private val heuristic: HeuristicFunction,
        private val numericComparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
) : Navigator {

    private val graph = NetworkBuilder
            .directed()
            .allowsSelfLoops(false)
            .allowsParallelEdges(false)
            .build<Id, Edge>()

    private val g = mutableMapOf<Id, Double>()
    private val rhs = mutableMapOf<Id, Double>()
    private val backtrack = mutableMapOf<Id, Id>()

    private var startNodeId = initialStartNodeId
    private var lastNodeId = startNodeId
    private var accumulatedHeuristic = 0.0

    private val keyComparator = Comparator<Key> { o1, o2 ->
        val r1 = numericComparator.compare(o1.k1, o2.k1)
        if (r1 != 0) {
            r1
        } else {
            numericComparator.compare(o1.k2, o2.k2)
        }
    }
    private val queue = PriorityQueue<ExpansionCandidate>(
            Comparator<ExpansionCandidate> { o1, o2 -> keyComparator.compare(o1.key, o2.key) }
    )
    private val nodeId2ExpansionCandidate = mutableMapOf<Id, ExpansionCandidate>()

    init {
        initialEdges.forEach { (from, to), cost ->
            val edge = Edge(from, to, cost)
            graph.addEdge(from, to, edge)
        }
        setRhs(goalNodeId, 0.0)
        val ec = ExpansionCandidate(goalNodeId, calculateKey(goalNodeId))
        queue.add(ec)
        nodeId2ExpansionCandidate.put(goalNodeId, ec)
        computeShortestPath()
    }

    override fun getShortestPathFrom(s: Id): List<Id> {
        if (startNodeId != s) {
            startNodeId = s
            accumulatedHeuristic += heuristic(lastNodeId, startNodeId)
            lastNodeId = startNodeId
            computeShortestPath()
        }
        val path = LinkedList<Id>()
        var node: Id? = startNodeId
        while (node != null) {
            path.addLast(node)
            node = backtrack[node]
        }
        return if (path.isEmpty() || path.last != goalNodeId) {
            emptyList()
        } else {
            path.toList()
        }
    }

    override fun addOrUpdateEdge(fromNodeId: Id, toNodeId: Id, cost: Double) {
        val edge = graph.edgeConnecting(fromNodeId, toNodeId).orElse(null)
        if (edge != null) {
            graph.removeEdge(edge)
        }
        graph.addEdge(fromNodeId, toNodeId, Edge(fromNodeId, toNodeId, cost))
        updateNode(fromNodeId)
        computeShortestPath()
    }

    override fun removeEdge(fromNodeId: Id, toNodeId: Id): Boolean {
        val edge = graph.edgeConnecting(fromNodeId, toNodeId).orElse(null) ?: return false
        graph.removeEdge(edge)
        updateNode(fromNodeId)
        computeShortestPath()
        return true
    }

    private fun computeShortestPath() {
        while (!isShortestPathComputed()) {
            val candidate = topExpansionCandidate()
            val nodeId = candidate.nodeId
            when {
                keyComparator.compare(candidate.key, calculateKey(nodeId)) < 0 -> {
                    addToQueue(nodeId)
                }
                numericComparator.compare(getG(nodeId), getRhs(nodeId)) > 0 -> {
                    setG(nodeId, getRhs(nodeId))
                    graph.predecessors(nodeId)?.forEach(this::updateNode)
                }
                else -> {
                    setG(nodeId, Double.POSITIVE_INFINITY)
                    graph.predecessors(nodeId)?.forEach(this::updateNode)
                    updateNode(nodeId)
                }
            }
        }
    }

    private fun isShortestPathComputed(): Boolean {
        val topKey = queue.peek()?.key ?: return true
        return keyComparator.compare(topKey, calculateKey(startNodeId)) >= 0 && isConsistent(startNodeId)
    }

    private fun updateNode(nodeId: Id) {
        removeFromQueueIfExists(nodeId)
        if (nodeId != goalNodeId) {
            updateRhs(nodeId)
        }
        if (!isConsistent(nodeId)) {
            addToQueue(nodeId)
        }
    }

    private fun topExpansionCandidate(): ExpansionCandidate {
        val candidate = queue.remove()
        nodeId2ExpansionCandidate.remove(candidate.nodeId)
        return candidate
    }

    private fun addToQueue(nodeId: Id) {
        removeFromQueueIfExists(nodeId)
        val candidate = ExpansionCandidate(nodeId, calculateKey(nodeId))
        queue.add(candidate)
        nodeId2ExpansionCandidate.put(nodeId, candidate)
    }

    private fun removeFromQueueIfExists(nodeId: Id): Boolean {
        val existingExpansionCandidate = nodeId2ExpansionCandidate[nodeId] ?: return false
        queue.remove(existingExpansionCandidate)
        nodeId2ExpansionCandidate.remove(nodeId)
        return true
    }

    private fun calculateKey(nodeId: Id): Key {
        val k2 = Math.min(getG(nodeId), getRhs(nodeId))
        val k1 = k2 + heuristic(startNodeId, nodeId) + accumulatedHeuristic
        return Key(k1, k2)
    }

    private fun updateRhs(nodeId: Id) {
        setRhs(nodeId, Double.POSITIVE_INFINITY)
        backtrack.remove(nodeId)
        graph.successors(nodeId)?.forEach { tryExpandEdge(nodeId, it) }
    }

    private fun tryExpandEdge(nodeId: Id, successorNodeId: Id) {
        val cost = graph.edgeConnecting(nodeId, successorNodeId).orElse(null)?.cost ?: Double.POSITIVE_INFINITY
        val rhsCandidate = cost + getG(successorNodeId)
        if (numericComparator.compare(rhsCandidate, getRhs(nodeId)) < 0) {
            setRhs(nodeId, rhsCandidate)
            backtrack.put(nodeId, successorNodeId)
        }
    }

    private fun isConsistent(nodeId: Id): Boolean {
        return numericComparator.compare(getG(nodeId), getRhs(nodeId)) == 0
    }

    private fun getG(nodeId: Id): Double {
        return g.getValueOrDefault(nodeId, Double.POSITIVE_INFINITY)
    }

    private fun setG(nodeId: Id, value: Double) {
        g.setFiniteValueOrRemove(nodeId, value)
    }

    private fun getRhs(nodeId: Id): Double {
        return rhs.getValueOrDefault(nodeId, Double.POSITIVE_INFINITY)
    }

    private fun setRhs(nodeId: Id, value: Double) {
        rhs.setFiniteValueOrRemove(nodeId, value)
    }

    private data class Edge(val fromNodeId: Id, val toNodeId: Id, val cost: Double)
    private data class ExpansionCandidate(val nodeId: Id, val key: Key)
    private data class Key(val k1: Double, val k2: Double)
}

private fun <K, V> Map<K, V>.getValueOrDefault(key: K, default: V): V {
    return this[key] ?: default
}

private fun <K> MutableMap<K, Double>.setFiniteValueOrRemove(key: K, value: Double) {
    if (value.isFinite()) {
        put(key, value)
    } else {
        remove(key)
    }
}
