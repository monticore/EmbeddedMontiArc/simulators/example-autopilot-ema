/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.geometry.Vector
import de.rwth.ryndin.modelling.autopilot.geometry.multiply
import de.rwth.ryndin.modelling.autopilot.geometry.normalize
import de.rwth.ryndin.modelling.autopilot.geometry.plus
import de.rwth.ryndin.modelling.autopilot.geometry.projectOnto
import de.rwth.ryndin.modelling.component.autopilot.behavior.CalculateNextDrivingPosition

class CalculateNextDrivingPositionImplementation(
        lookAhead: Double
) : CalculateNextDrivingPosition(lookAhead) {

    override fun execute() {
        val givenCurrentPosition = currentPosition ?: return
        val givenP1 = p1 ?: return
        val givenP2 = p2 ?: return
        val dir = Vector.create(givenP1, givenP2).normalize()
        val p = givenCurrentPosition.projectOnto(givenP1, givenP2)
        setNext(p.plus(dir.multiply(lookAhead)))
    }
}
