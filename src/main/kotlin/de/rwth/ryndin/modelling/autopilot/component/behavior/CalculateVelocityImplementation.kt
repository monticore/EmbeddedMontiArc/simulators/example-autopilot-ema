/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.DEFAULT_NUMERIC_COMPARATOR
import de.rwth.ryndin.modelling.component.autopilot.behavior.CalculateVelocity

class CalculateVelocityImplementation(
        comparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
) : CalculateVelocity() {

    private val calculateVelocity = Distance2VelocityFunction(comparator)

    override fun execute() {
        val d = distanceToFinish ?: return
        setVelocity(calculateVelocity(d))
    }
}
