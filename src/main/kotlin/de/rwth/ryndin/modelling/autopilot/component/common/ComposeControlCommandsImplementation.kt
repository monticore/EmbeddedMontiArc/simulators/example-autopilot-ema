/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.ComposeControlCommands
import de.rwth.ryndin.modelling.structure.autopilot.Actuation
import de.rwth.ryndin.modelling.structure.autopilot.ControlCommands

class ComposeControlCommandsImplementation(
        defaultEngine: Double,
        defaultSteering: Double,
        defaultBrakes: Double
) : ComposeControlCommands(defaultEngine, defaultSteering, defaultBrakes) {

    override fun execute() {
        val e = engine ?: defaultEngine
        val s = steering ?: defaultSteering
        val b = brakes ?: defaultBrakes
        setControlCommands(ControlCommands(Actuation(
                engine = e,
                steering = s,
                brakes = b
        )))
    }
}
