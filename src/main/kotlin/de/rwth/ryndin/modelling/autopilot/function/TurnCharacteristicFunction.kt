/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.function

import de.rwth.ryndin.modelling.autopilot.DEFAULT_NUMERIC_COMPARATOR
import java.util.PriorityQueue

internal class TurnCharacteristicFunction(
        data: Iterable<TurnCharacteristicMeasurement>,
        private val comparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
) {

    private val measurementData: List<TurnCharacteristicMeasurement> = data.sorted()

    init {
        measurementData.forEachIndexed { i, m ->
            if (i > 0) {
                val prev = measurementData[i - 1]
                if (isSameVelocity(m, prev) && isSameSteering(m, prev)) {
                    throw IllegalArgumentException(
                            "corrupt measurement data: $m and $prev"
                    )
                }
            }
        }
    }

    fun getBestFit(radiusRange: ClosedFloatingPointRange<Double>): TurnCharacteristicMeasurement? {
        val r = (radiusRange.start + radiusRange.endInclusive) / 2
        val candidates = PriorityQueue<Candidate>()
        measurementData.forEachIndexed { i, m ->
            if (radiusRange.contains(m.radius)) {
                val degreeOfFreedom = getDegreeOfFreedom(radiusRange, i)
                val radiusDifference = Math.abs(r - m.radius)
                candidates.add(Candidate(m, degreeOfFreedom, radiusDifference))
            }
        }
        if (candidates.isEmpty()) {
            return null
        }
        return candidates.remove().measurement
    }

    private fun getDegreeOfFreedom(r: ClosedFloatingPointRange<Double>, i: Int): Int {
        val m = measurementData[i]
        var result = 0
        if (i > 0) {
            val prev = measurementData[i - 1]
            if (isSuitable(r, m, prev)) {
                assert(!isSameSteering(m, prev))
                result++
            }
        }
        if (i < measurementData.lastIndex) {
            val next = measurementData[i - 1]
            if (isSuitable(r, m, next)) {
                assert(!isSameSteering(m, next))
                result++
            }
        }
        return result
    }

    private fun isSuitable(
            range: ClosedFloatingPointRange<Double>,
            baseMeasurement: TurnCharacteristicMeasurement,
            measurement: TurnCharacteristicMeasurement
    ): Boolean {
        return range.contains(measurement.radius) && isSameVelocity(baseMeasurement, measurement)
    }

    private fun isSameVelocity(
            m1: TurnCharacteristicMeasurement,
            m2: TurnCharacteristicMeasurement
    ): Boolean {
        return comparator.compare(m1.velocity, m2.velocity) == 0
    }

    private fun isSameSteering(
            m1: TurnCharacteristicMeasurement,
            m2: TurnCharacteristicMeasurement
    ): Boolean {
        return comparator.compare(m1.steering, m2.steering) == 0
    }

    private data class Candidate(
            val measurement: TurnCharacteristicMeasurement,
            val degreeOfFreedom: Int,
            val radiusDifference: Double
    ) : Comparable<Candidate> {
        override fun compareTo(other: Candidate): Int {
            val r1 = -1 * degreeOfFreedom.compareTo(other.degreeOfFreedom)
            if (r1 != 0) {
                return r1
            }
            val r2 = -1 * measurement.velocity.compareTo(other.measurement.velocity)
            if (r2 != 0) {
                return r2
            }
            return radiusDifference.compareTo(other.radiusDifference)
        }
    }
}

internal data class TurnCharacteristicMeasurement(
        val radius: Double,
        val steering: Double,
        val velocity: Double
) : Comparable<TurnCharacteristicMeasurement> {
    override fun compareTo(other: TurnCharacteristicMeasurement): Int {
        val r1 = velocity.compareTo(other.velocity)
        if (r1 != 0) {
            return r1
        }
        val r2 = steering.compareTo(other.steering)
        if (r2 != 0) {
            return r2
        }
        return radius.compareTo(other.radius)
    }
}
