/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.component.autopilot.planning.ActivateNavigator

class ActivateNavigatorImplementation : ActivateNavigator() {
    override fun execute() {
        if (isMapDataChanged == true || isRecalculatePath == true) {
            val start = inputStartNodeId ?: return
            setOutputStartNodeId(start)
        }
    }
}
