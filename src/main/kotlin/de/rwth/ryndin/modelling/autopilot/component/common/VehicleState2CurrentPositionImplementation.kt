/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.VehicleState2CurrentPosition

class VehicleState2CurrentPositionImplementation : VehicleState2CurrentPosition() {
    override fun execute() {
        val vs = vehicleState ?: return
        setCurrentPosition(vs.sensorData.position)
    }
}
