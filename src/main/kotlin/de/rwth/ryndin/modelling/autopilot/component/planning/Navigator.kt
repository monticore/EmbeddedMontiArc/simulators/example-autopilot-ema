/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

internal typealias Id = Long
internal typealias HeuristicFunction = (Id, Id) -> Double

internal interface Navigator {
    val goalNodeId: Id
    fun getShortestPathFrom(s: Id): List<Id>
    fun addOrUpdateEdge(fromNodeId: Id, toNodeId: Id, cost: Double)
    fun removeEdge(fromNodeId: Id, toNodeId: Id): Boolean
}
