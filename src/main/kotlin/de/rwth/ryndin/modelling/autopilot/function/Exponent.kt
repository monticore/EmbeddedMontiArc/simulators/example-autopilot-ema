/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.function

internal class Exponent(
        private val x1: Double,
        private val y1: Double,
        private val x2: Double,
        private val y2: Double
) : (Double) -> Double {

    private val k: Double

    init {
        if (x1 >= x2) {
            throw IllegalArgumentException("x1 must be less than x2")
        }
        if (y1 <= 0.0) {
            throw IllegalArgumentException("y1 must be positive")
        }
        if (y2 <= 0.0) {
            throw IllegalArgumentException("y2 must be positive")
        }
        k = Math.log(y1 / y2) / (x2 - x1)
    }

    override fun invoke(x: Double): Double {
        if (x < x1) {
            return y1
        }
        if (x > x2) {
            return y2
        }
        return y1 * Math.exp(-k * (x - x1))
    }
}
