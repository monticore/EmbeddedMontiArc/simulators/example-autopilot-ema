/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.PID

class PIDImplementation : PID() {

    private var lastError: Double? = null
    private var integral = 0.0

    override fun execute() {
        val p = paramP ?: 0.0
        val i = paramI ?: 0.0
        val d = paramD ?: 0.0
        val decay = paramDecayCoefficient ?: 0.0
        val e = error ?: 0.0
        val derivative = e - (lastError ?: e)
        lastError = e
        integral = decay * integral + e
        val control = p * e + i * integral + d * derivative
        setControl(control)
    }
}
