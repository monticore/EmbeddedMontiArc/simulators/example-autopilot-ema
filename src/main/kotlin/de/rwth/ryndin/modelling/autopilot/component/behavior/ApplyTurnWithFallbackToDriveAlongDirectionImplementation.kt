/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.DEFAULT_NUMERIC_COMPARATOR
import de.rwth.ryndin.modelling.autopilot.function.Exponent
import de.rwth.ryndin.modelling.autopilot.geometry.Vector
import de.rwth.ryndin.modelling.autopilot.geometry.calculateSignedAngleBetween
import de.rwth.ryndin.modelling.autopilot.geometry.currentDirection
import de.rwth.ryndin.modelling.autopilot.geometry.distanceTo
import de.rwth.ryndin.modelling.component.autopilot.behavior.ApplyTurnWithFallbackToDriveAlongDirection

class ApplyTurnWithFallbackToDriveAlongDirectionImplementation(
        private val comparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
) : ApplyTurnWithFallbackToDriveAlongDirection() {

    companion object {
        private val D1 = 0.05 // m
        private val D2 = 2.0 // m
        private val RADIUS_DIFFERENCE_2_CORRECTION_COEFFICIENT = Exponent(
                D1, // m
                0.05,
                D2, // m
                0.5
        )
    }

    override fun execute() {
        val state = vehicleState ?: return
        val t = turnMotionData ?: return
        val turnDir = Vector.create(t.middle, t.end)
        val directionalSteeringAngle = calculateSignedAngleBetween(state.currentDirection, turnDir)
        if (directionalSteeringAngle * t.steeringAngle >= 0.0
                && comparator.compare(Math.abs(directionalSteeringAngle), Math.abs(t.steeringAngle)) <= 0) {
            setFallbackCurrentPosition(state.sensorData.position)
            setFallbackP1(t.middle)
            setFallbackP2(t.end)
            setFallbackVelocity(t.velocity)
            return
        }
        val currentRadius = state.sensorData.position.distanceTo(t.associatedCircle.center)
        val d = currentRadius - t.associatedCircle.radius
        val correction = if (comparator.compare(Math.abs(d), D1) >= 0) {
            val k = RADIUS_DIFFERENCE_2_CORRECTION_COEFFICIENT(Math.abs(d))
            Math.signum(d) * k * t.steeringAngle
        } else {
            0.0
        }
        setDesiredSteeringAngle(t.steeringAngle + correction)
        setDesiredVelocity(t.velocity)
    }
}
