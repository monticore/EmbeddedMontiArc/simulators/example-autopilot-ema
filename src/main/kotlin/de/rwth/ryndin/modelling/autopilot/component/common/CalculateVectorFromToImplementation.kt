/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.geometry.Vector
import de.rwth.ryndin.modelling.autopilot.geometry.toStruct
import de.rwth.ryndin.modelling.component.autopilot.common.CalculateVectorFromTo

class CalculateVectorFromToImplementation : CalculateVectorFromTo() {
    override fun execute() {
        val givenFrom = from ?: return
        val givenTo = to ?: return
        val v = Vector.create(givenFrom, givenTo)
        setVector(v.toStruct())
    }
}
