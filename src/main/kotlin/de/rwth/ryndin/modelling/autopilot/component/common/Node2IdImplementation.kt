/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.Node2Id

class Node2IdImplementation : Node2Id() {
    override fun execute() {
        if (node != null) {
            setId(node!!.id)
        }
    }
}
