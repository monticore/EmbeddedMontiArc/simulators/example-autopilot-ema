/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.Abs

class AbsImplementation : Abs() {
    override fun execute() {
        val value = input ?: return
        if (!value.isNaN()) {
            setOutput(Math.abs(value))
        }
    }
}
