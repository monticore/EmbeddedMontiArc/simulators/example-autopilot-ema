/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.geometry

import de.rwth.ryndin.modelling.structure.autopilot.Position

internal data class Circle(
        val center: Position,
        val radius: Double
)

internal fun Circle.toStruct(): de.rwth.ryndin.modelling.structure.autopilot.Circle {
    return de.rwth.ryndin.modelling.structure.autopilot.Circle(
            center = center,
            radius = radius
    )
}
