/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.autopilot.geometry.distanceTo
import de.rwth.ryndin.modelling.structure.autopilot.Node

internal class EuclideanHeuristic(
        private val nodes: Map<Id, Node>,
        private val defaultValue: Double = 0.0
) : HeuristicFunction {
    override fun invoke(from: Id, to: Id): Double {
        val p1 = nodes[from]?.position ?: return defaultValue
        val p2 = nodes[to]?.position ?: return defaultValue
        return p1.distanceTo(p2)
    }
}
