/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.motion

import de.rwth.ryndin.modelling.component.autopilot.motion.CalculatePidParameters

class CalculatePidParametersImplementation : CalculatePidParameters() {

    companion object {
        private val MIN_VELOCITY = 0.5
        private val MAX_VELOCITY = 13.0 - MIN_VELOCITY / 10.0
        private val P_FOR_MIN_VELOCITY = 1.76703
        private val P_FOR_MAX_VELOCITY = 3.29578
    }

    override fun execute() {
        setParamP(0.0)
        setParamI(0.0)
        setParamD(0.0)
        setParamDecayCoefficient(0.0)
        val cv = currentVelocity ?: Double.NaN
        val dv = desiredVelocity ?: Double.NaN
        if (!cv.isNaN() && !dv.isNaN()) {
            val v = Math.min(cv, dv)
            val p = when {
                v <= MIN_VELOCITY -> P_FOR_MIN_VELOCITY
                v >= MAX_VELOCITY -> P_FOR_MAX_VELOCITY
                else -> {
                    val diff = v - MIN_VELOCITY
                    P_FOR_MIN_VELOCITY + (P_FOR_MAX_VELOCITY - P_FOR_MIN_VELOCITY) * diff / (MAX_VELOCITY - MIN_VELOCITY)
                }
            }
            setParamP(p)
        }
    }
}
