/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.geometry

import de.rwth.ryndin.modelling.autopilot.DEFAULT_NUMERIC_COMPARATOR
import de.rwth.ryndin.modelling.structure.autopilot.Position

internal data class Vector(val x: Double, val y: Double) {
    companion object {
        fun create(from: Position, to: Position): Vector {
            return Vector(to.x - from.x, to.y - from.y)
        }
    }
}

internal fun de.rwth.ryndin.modelling.structure.autopilot.Vector.toModel(): Vector {
    return Vector(x, y)
}

internal fun Vector.toStruct(): de.rwth.ryndin.modelling.structure.autopilot.Vector {
    return de.rwth.ryndin.modelling.structure.autopilot.Vector(x, y)
}

internal val Vector.angle: Double
    get() = Math.atan2(y, x)

internal val Vector.norm: Double
    get() = Math.sqrt(squareNorm)

internal val Vector.squareNorm: Double
    get() = x * x + y * y

internal fun Vector.normalize(): Vector {
    val n = norm
    return Vector(x / n, y / n)
}

internal fun Vector.plus(v: Vector): Vector {
    return Vector(x + v.x, y + v.y)
}

internal fun Vector.dotProduct(v: Vector): Double {
    return x * v.x + y * v.y
}

internal fun Vector.multiply(k: Double): Vector {
    return Vector(k * x, k * y)
}

internal fun Vector.rotate(angle: Double): Vector {
    val cos = Math.cos(angle)
    val sin = Math.sin(angle)
    return Vector(x * cos - y * sin, x * sin + y * cos)
}

internal fun Position.plus(v: Vector): Position {
    return Position(x + v.x, y + v.y)
}

internal fun calculateTurnAngle(
        p1: Position,
        p2: Position,
        p3: Position
): Double {
    return Math.abs(calculateSignedTurnAngle(p1, p2, p3))
}

internal fun calculateSignedTurnAngle(
        p1: Position,
        p2: Position,
        p3: Position
): Double {
    return calculateSignedAngleBetween(
            Vector.create(p1, p2),
            Vector.create(p2, p3)
    )
}

internal fun calculateSignedAngleBetween(
        v1: Vector,
        v2: Vector,
        comparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
): Double {
    if (comparator.compare(Math.abs(v1.norm), 0.0) == 0 || comparator.compare(Math.abs(v2.norm), 0.0) == 0) {
        return 0.0
    }
    val rotatedV2 = v2.rotate(-v1.angle)
    val angle = -rotatedV2.angle
    return if (comparator.compare(Math.abs(angle), Math.PI) == 0)
        -Math.PI
    else
        angle
}
