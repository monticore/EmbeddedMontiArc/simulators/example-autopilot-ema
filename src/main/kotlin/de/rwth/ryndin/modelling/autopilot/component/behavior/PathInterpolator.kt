/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import com.fasterxml.jackson.databind.ObjectMapper
import de.rwth.ryndin.modelling.autopilot.DEFAULT_NUMERIC_COMPARATOR
import de.rwth.ryndin.modelling.autopilot.function.TurnCharacteristicFunction
import de.rwth.ryndin.modelling.autopilot.geometry.Circle
import de.rwth.ryndin.modelling.autopilot.geometry.Vector
import de.rwth.ryndin.modelling.autopilot.geometry.bezierInterpolation
import de.rwth.ryndin.modelling.autopilot.geometry.calculateSignedAngleBetween
import de.rwth.ryndin.modelling.autopilot.geometry.calculateSignedTurnAngle
import de.rwth.ryndin.modelling.autopilot.geometry.calculateTurnAngle
import de.rwth.ryndin.modelling.autopilot.geometry.distanceTo
import de.rwth.ryndin.modelling.autopilot.geometry.interpolateLine
import de.rwth.ryndin.modelling.autopilot.geometry.multiply
import de.rwth.ryndin.modelling.autopilot.geometry.norm
import de.rwth.ryndin.modelling.autopilot.geometry.normalize
import de.rwth.ryndin.modelling.autopilot.geometry.plus
import de.rwth.ryndin.modelling.autopilot.geometry.rotate
import de.rwth.ryndin.modelling.structure.autopilot.Position
import java.util.LinkedList

internal class PathInterpolator(
        private val turnAngleThreshold: Double,
        private val unfeasibleTurnAngleThreshold: Double,
        private val comparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
) {
    companion object {
        private val TURN_PRECISION = 1.5 // m
        private val MIN_TURN_RADIUS = 3.5 // m
        private val MAX_TURN_RADIUS = 60.0 // m
        private val SEGMENT_LENGTH = 1.0 // m
        private val TURN_CHARACTERISTIC_FUNCTION: TurnCharacteristicFunction

        init {
            val reader = javaClass.classLoader.getResourceAsStream("radius-steering-velocity.json").reader()
            val mapper = ObjectMapper()
            val dataType = mapper.typeFactory.constructCollectionType(List::class.java, TurnCharacteristicMeasurementDto::class.java)
            val data = mapper.readValue<List<TurnCharacteristicMeasurementDto>>(reader, dataType)
                    .map(TurnCharacteristicMeasurementDto::toTurnCharacteristicMeasurement)
            TURN_CHARACTERISTIC_FUNCTION = TurnCharacteristicFunction(data)
        }
    }

    private val interpolated = LinkedList<Position>()
    private val behavior = LinkedList<BehaviorModel>()

    fun interpolate(path: List<Position>): Pair<List<Position>, List<BehaviorModel>> {
        when {
            path.isEmpty() -> return Pair(emptyList(), emptyList())
            path.size == 1 -> {
                val p = path[0]
                return Pair(listOf(p), listOf(DriveToPosition(p)))
            }
        }
        val angles = path.mapIndexed { i, p2 ->
            when {
                i == 0 || i >= path.lastIndex -> 0.0
                else -> {
                    val p1 = path[i - 1]
                    val p3 = path[i + 1]
                    calculateTurnAngle(p1, p2, p3)
                }
            }
        }
        interpolated.clear()
        behavior.clear()
        interpolated.add(path[0])
        behavior.add(KeepDirection(Vector(0.0, 0.0)))
        var i = 2
        while (i < path.size) {
            val p1 = interpolated.last
            val p2 = path[i - 1]
            if (comparator.compare(p1.distanceTo(p2), SEGMENT_LENGTH) < 0) {
                i++
                continue
            }
            val p3 = path[i]
            val isLast = i >= path.lastIndex
            val isNextStraightSegment = !isTurn(angles[i])
            val turnAngle = angles[i - 1]
            if (isTurn(turnAngle)) {
                val v21 = Vector.create(p2, p1)
                val v23 = Vector.create(p2, p3)
                val reserveLeft = v21.norm
                val reserveRight = if (isLast || isNextStraightSegment) {
                    v23.norm
                } else {
                    v23.norm / 2.0
                }
                val reserve = minOf(reserveLeft, reserveRight)
                val start = p2.plus(v21.normalize().multiply(reserve))
                val end = p2.plus(v23.normalize().multiply(reserve))
                if (comparator.compare(p1.distanceTo(start), SEGMENT_LENGTH) > 0) {
                    straightSegment(p1, start)
                }
                if (isUnfeasibleTurn(turnAngle)) {
                    unfeasibleTurn(start, p2, end)
                } else {
                    possibleTurn(start, p2, end)
                }
            } else {
                straightSegment(p1, p2)
            }
            i++
        }
        val lastP2 = interpolated.last
        val lastP3 = path.last()
        val v23 = Vector.create(lastP2, lastP3)
        if (comparator.compare(v23.norm, SEGMENT_LENGTH) > 0) {
            val reserveBeforeFinish = 5.0
            if (comparator.compare(v23.norm, reserveBeforeFinish) <= 0) {
                addLinearSegment(lastP2, lastP3, DriveToPosition(lastP3))
            } else {
                val v23n = v23.normalize()
                val intermediate = lastP2.plus(v23n.multiply(reserveBeforeFinish))
                addLinearSegment(lastP2, intermediate, KeepDirection(v23n))
                addLinearSegment(intermediate, lastP3, DriveToPosition(lastP3))
            }
        }
        return Pair(interpolated.toList(), behavior.toList())
    }

    private fun possibleTurn(p1: Position, p2: Position, p3: Position) {
        val dMax = p1.distanceTo(p2)
        val signedTurnAngle = calculateSignedTurnAngle(p1, p2, p3)
        val turnAngle = Math.abs(signedTurnAngle)
        val beta = turnAngle / 2
        val sinBeta = Math.sin(beta)
        val cosBeta = Math.cos(beta)
        val tanBeta = Math.tan(beta)
        val lMax = dMax * (1 - cosBeta) / sinBeta
        val lMin = minOf(TURN_PRECISION, lMax)
        val k = 1 / cosBeta - 1
        val rMin = lMin / k
        val rMax = lMax / k
        if (rMax < MIN_TURN_RADIUS) {
            unfeasibleTurn(p1, p2, p3)
            return
        }
        if (MAX_TURN_RADIUS < rMin) {
            straightSegment(p1, p2)
            straightSegment(p2, p3)
            return
        }
        val effectiveR1 = maxOf(MIN_TURN_RADIUS, rMin)
        val effectiveR2 = minOf(MAX_TURN_RADIUS, rMax)
        val radiusRange = effectiveR1.rangeTo(effectiveR2)
        val bestFit = TURN_CHARACTERISTIC_FUNCTION.getBestFit(radiusRange)
        if (bestFit == null) {
            unfeasibleTurn(p1, p2, p3)
            return
        }
        val reserve = bestFit.radius * tanBeta
        assert(reserve <= dMax)
        val v21 = Vector.create(p2, p1).normalize()
        val v23 = Vector.create(p2, p3).normalize()
        val center = p2.plus(
                v21.plus(v23).normalize().multiply(reserve / sinBeta)
        )
        val start = p2.plus(v21.multiply(reserve))
        val end = p2.plus(v23.multiply(reserve))
        if (comparator.compare(p1.distanceTo(start), SEGMENT_LENGTH) > 0) {
            straightSegment(p1, start)
        }
        val vcs = Vector.create(center, start)
        val vce = Vector.create(center, end)
        val diff = -calculateSignedAngleBetween(vcs, vce)
        val curveLen = Math.abs(diff) * bestFit.radius
        val numberOfSegments = (Math.floor(curveLen / SEGMENT_LENGTH)).toInt()
        val chunk = if (numberOfSegments > 1) {
            val c = mutableListOf(start)
            (1 until numberOfSegments).forEach {
                val v = vcs.rotate((diff * it) / numberOfSegments)
                c.add(center.plus(v))
            }
            c.add(end)
            c
        } else {
            listOf(start, end)
        }
        val b = Turn(
                start,
                p2,
                end,
                bestFit.velocity,
                Math.signum(signedTurnAngle) * bestFit.steering,
                Circle(center, bestFit.radius)
        )
        ensureJoint(start)
        chunk.forEach {
            interpolated.add(it)
            behavior.add(b)
        }
        if (comparator.compare(end.distanceTo(p3), SEGMENT_LENGTH) > 0) {
            straightSegment(end, p3)
        }
    }

    private fun unfeasibleTurn(p1: Position, p2: Position, p3: Position) {
        val chunk = bezierInterpolation(p1, p2, p3, SEGMENT_LENGTH, comparator)
        ensureJoint(p1)
        chunk.forEachIndexed { i, p ->
            interpolated.add(p)
            val next = if (i >= chunk.lastIndex) {
                p
            } else {
                chunk[i + 1]
            }
            behavior.add(DriveToPosition(next))
        }
    }

    private fun straightSegment(p1: Position, p2: Position) {
        val b = KeepDirection(Vector.create(p1, p2).normalize())
        addLinearSegment(p1, p2, b)
    }

    private fun addLinearSegment(p1: Position, p2: Position, b: BehaviorModel) {
        val chunk = interpolateLine(p1, p2, SEGMENT_LENGTH, comparator)
        ensureJoint(p1)
        chunk.forEach {
            interpolated.add(it)
            behavior.add(b)
        }
    }

    private fun ensureJoint(p: Position) {
        when {
            interpolated.isEmpty() -> return
            comparator.compare(p.distanceTo(interpolated.last), SEGMENT_LENGTH / 2) <= 0 -> {
                interpolated.removeFirst()
                behavior.removeFirst()
            }
        }
    }

    private fun isTurn(turnAngle: Double): Boolean {
        return comparator.compare(turnAngle, turnAngleThreshold) >= 0
    }

    private fun isUnfeasibleTurn(turnAngle: Double): Boolean {
        return comparator.compare(turnAngle, unfeasibleTurnAngleThreshold) >= 0
    }
}
