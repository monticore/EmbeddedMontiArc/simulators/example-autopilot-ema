/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.Condition

class ConditionImplementation : Condition<Any>() {
    override fun execute() {
        val value = if (condition == true) {
            value1
        } else {
            value2
        }
        if (value != null) {
            setOutput(value)
        }
    }
}
