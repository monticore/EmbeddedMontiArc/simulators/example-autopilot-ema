/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.geometry.Circle
import de.rwth.ryndin.modelling.autopilot.geometry.Vector
import de.rwth.ryndin.modelling.autopilot.geometry.toStruct
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.TurnMotionData

internal interface BehaviorModel

internal class KeepDirection(val direction: Vector) : BehaviorModel

internal class DriveToPosition(val targetPosition: Position) : BehaviorModel

internal class Turn(
        val start: Position,
        val middle: Position,
        val end: Position,
        val velocity: Double,
        val steeringAngle: Double,
        val associatedCircle: Circle
) : BehaviorModel

internal fun Turn.toStruct(): TurnMotionData {
    return TurnMotionData(
            start,
            middle,
            end,
            velocity,
            steeringAngle,
            associatedCircle.toStruct()
    )
}
