/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.perception

import de.rwth.ryndin.modelling.component.autopilot.perception.Perception
import de.rwth.ryndin.modelling.structure.autopilot.VehicleState

internal class PerceptionImplementation : Perception() {
    override fun execute() {
        val a = actuation ?: return
        val sd = sensorData ?: return
        setVehicleState(VehicleState(a, sd))
    }
}
