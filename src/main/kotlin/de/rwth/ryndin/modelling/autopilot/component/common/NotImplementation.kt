/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.Not

class NotImplementation : Not() {
    override fun execute() {
        val i = input ?: return
        setOutput(!i)
    }
}
