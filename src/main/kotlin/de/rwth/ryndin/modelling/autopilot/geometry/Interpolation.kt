/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.geometry

import de.rwth.ryndin.modelling.autopilot.DEFAULT_NUMERIC_COMPARATOR
import de.rwth.ryndin.modelling.structure.autopilot.Position

internal fun interpolateLine(
        from: Position,
        to: Position,
        segmentLength: Double,
        comparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
): List<Position> {
    val result = mutableListOf(from)
    var remainder = result.last().distanceTo(to)
    val v = Vector.create(from, to).normalize()
    while (comparator.compare(remainder, 0.0) > 0) {
        if (comparator.compare(remainder, segmentLength) <= 0) {
            result.add(to)
        } else {
            result.add(result.last().plus(v.multiply(segmentLength)))
        }
        remainder = result.last().distanceTo(to)
    }
    return result
}

internal fun bezierInterpolation(
        p1: Position,
        p2: Position,
        p3: Position,
        segmentLength: Double,
        comparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
): List<Position> {
    fun interpolate(t1: Double, t2: Double): List<Position> {
        val ip1 = calculateBezierPoint(p1, p2, p3, t1)
        val ip2 = calculateBezierPoint(p1, p2, p3, t2)
        if (comparator.compare(t1, t2) >= 0 || comparator.compare(ip1.distanceTo(ip2), segmentLength) <= 0) {
            return listOf(ip1, ip2)
        }
        val m = (t1 + t2) / 2
        val left = interpolate(t1, m)
        val right = interpolate(m, t2)
        return left.dropLast(1).plus(right)
    }

    val result = interpolate(0.0, 1.0)
    return listOf(p1)
            .plus(result.drop(1).dropLast(1))
            .plus(p3)
}

internal fun calculateBezierPoint(
        p1: Position,
        p2: Position,
        p3: Position,
        t: Double
): Position {
    val a = 1.0 - t
    val b = t
    val k1 = a * a
    val k2 = 2 * a * b
    val k3 = b * b
    val x = k1 * p1.x + k2 * p2.x + k3 * p3.x
    val y = k1 * p1.y + k2 * p2.y + k3 * p3.y
    return Position(x, y)
}
