/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.EnsureBounds

class EnsureBoundsImplementation(
        min: Double,
        max: Double
) : EnsureBounds(min, max) {
    override fun execute() {
        val value = input ?: return
        if (!value.isNaN()) {
            val output = when {
                value < min -> min
                value > max -> max
                else -> value
            }
            setOutput(output)
        }
    }
}
