/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot

internal val DEFAULT_NUMERIC_COMPARATOR = NumericComparator(1e-3)

internal class NumericComparator(private val precision: Double) : Comparator<Double> {
    override fun compare(o1: Double, o2: Double): Int {
        if (!o1.isFinite() || !o2.isFinite()) {
            return o1.compareTo(o2)
        }
        val diff = o1 - o2
        if (Math.abs(diff) <= precision) {
            return 0
        }
        return if (diff < 0.0) {
            -1
        } else {
            1
        }
    }
}

internal fun Double.toRadians(): Double {
    return this * Math.PI / 180.0
}
