/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import com.google.common.graph.NetworkBuilder
import java.util.LinkedList
import java.util.PriorityQueue

internal class DijkstraBasedNavigator(
        initialEdges: Map<Pair<Id, Id>, Double>,
        override val goalNodeId: Id
) : Navigator {

    private val graph = NetworkBuilder
            .directed()
            .allowsSelfLoops(false)
            .allowsParallelEdges(false)
            .build<Id, Edge>()

    private val queue = PriorityQueue<DistanceEstimate>()
    private val nodeId2Estimate = mutableMapOf<Id, DistanceEstimate>()
    private val processedNodes = mutableSetOf<Id>()
    private val backtrack = mutableMapOf<Id, Id>()

    init {
        initialEdges.forEach { (from, to), cost ->
            val edge = Edge(from, to, cost)
            graph.addEdge(from, to, edge)
        }
    }

    override fun getShortestPathFrom(s: Id): List<Id> {
        queue.clear()
        nodeId2Estimate.clear()
        processedNodes.clear()
        backtrack.clear()
        val startNodeEstimate = DistanceEstimate(s, 0.0)
        queue.add(startNodeEstimate)
        nodeId2Estimate.put(s, startNodeEstimate)
        while (queue.isNotEmpty()) {
            val estimate = queue.remove()
            nodeId2Estimate.remove(estimate.nodeId)
            processedNodes.add(estimate.nodeId)
            if (estimate.nodeId == goalNodeId) {
                val path = LinkedList<Id>()
                var node: Id? = goalNodeId
                while (node != null) {
                    path.addFirst(node)
                    node = backtrack[node]
                }
                return path.toList()
            }
            graph.outEdges(estimate.nodeId)?.forEach {
                tryImproveDistanceEstimate(estimate, it.toNodeId, it.cost)
            }
        }
        return emptyList()
    }

    override fun addOrUpdateEdge(fromNodeId: Id, toNodeId: Id, cost: Double) {
        val edge = graph.edgeConnecting(fromNodeId, toNodeId).orElse(null)
        if (edge != null) {
            graph.removeEdge(edge)
        }
        graph.addEdge(fromNodeId, toNodeId, Edge(fromNodeId, toNodeId, cost))
    }

    override fun removeEdge(fromNodeId: Id, toNodeId: Id): Boolean {
        val edge = graph.edgeConnecting(fromNodeId, toNodeId).orElse(null) ?: return false
        graph.removeEdge(edge)
        return true
    }

    private fun tryImproveDistanceEstimate(distanceEstimate: DistanceEstimate, toNodeId: Id, edgeCost: Double) {
        if (processedNodes.contains(toNodeId)) {
            return
        }
        val distanceCandidate = distanceEstimate.distance + edgeCost
        val existingDistanceEstimate = nodeId2Estimate[toNodeId]
        if (existingDistanceEstimate != null) {
            if (distanceCandidate < existingDistanceEstimate.distance) {
                queue.remove(existingDistanceEstimate)
            } else {
                return
            }
        }
        val newDistanceEstimate = DistanceEstimate(toNodeId, distanceCandidate)
        nodeId2Estimate[toNodeId] = newDistanceEstimate
        queue.add(newDistanceEstimate)
        backtrack[toNodeId] = distanceEstimate.nodeId
    }

    private data class Edge(
            val fromNodeId: Id,
            val toNodeId: Id,
            val cost: Double
    )

    private data class DistanceEstimate(val nodeId: Id, val distance: Double) : Comparable<DistanceEstimate> {
        override fun compareTo(other: DistanceEstimate): Int {
            return distance.compareTo(other.distance)
        }
    }
}
