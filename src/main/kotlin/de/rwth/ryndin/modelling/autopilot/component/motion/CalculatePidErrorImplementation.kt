/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.motion

import de.rwth.ryndin.modelling.component.autopilot.motion.CalculatePidError

class CalculatePidErrorImplementation(
        velocityDifferenceThresholdForErrorLeap: Double
) : CalculatePidError(velocityDifferenceThresholdForErrorLeap) {

    companion object {
        private val ERROR_TO_INDUCE_BRAKING = -100.0
    }

    override fun execute() {
        val cv = currentVelocity ?: Double.NaN
        val dv = desiredVelocity ?: Double.NaN
        val error = when {
            cv.isNaN() || dv.isNaN() -> 0.0
            dv <= 1e-3 -> ERROR_TO_INDUCE_BRAKING
            cv - dv > velocityDifferenceThresholdForErrorLeap -> -cv
            dv - cv > velocityDifferenceThresholdForErrorLeap -> dv
            else -> dv - cv
        }
        setError(error)
    }
}
