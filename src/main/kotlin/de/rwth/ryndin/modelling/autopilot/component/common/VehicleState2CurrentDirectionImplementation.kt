/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.geometry.currentDirection
import de.rwth.ryndin.modelling.autopilot.geometry.toStruct
import de.rwth.ryndin.modelling.component.autopilot.common.VehicleState2CurrentDirection

class VehicleState2CurrentDirectionImplementation : VehicleState2CurrentDirection() {
    override fun execute() {
        val vs = vehicleState ?: return
        setCurrentDirection(vs.currentDirection.toStruct())
    }
}
