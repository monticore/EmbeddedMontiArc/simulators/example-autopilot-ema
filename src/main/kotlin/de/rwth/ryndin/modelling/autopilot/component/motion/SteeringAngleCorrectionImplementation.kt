/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.motion

import de.rwth.ryndin.modelling.autopilot.function.Exponent
import de.rwth.ryndin.modelling.component.autopilot.motion.SteeringAngleCorrection

class SteeringAngleCorrectionImplementation(
        maxSteeringAngle: Double
) : SteeringAngleCorrection(maxSteeringAngle) {

    private val distanceError2SteeringAngle = Exponent(
            0.1, // m
            0.01 * maxSteeringAngle, // rad
            5.0, // m
            0.05 * maxSteeringAngle // rad
    )

    override fun execute() {
        val sdtt = signedDistanceToTrajectory ?: 0.0
        val d = Math.abs(sdtt)
        if (d <= 1e-2) {
            setSteeringAngleCorrection(0.0)
        } else {
            setSteeringAngleCorrection(Math.signum(sdtt) * distanceError2SteeringAngle(d))
        }
    }
}
