/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.EnsureBoundsDynamic

class EnsureBoundsDynamicImplementation : EnsureBoundsDynamic() {
    override fun execute() {
        val i = input ?: return
        if (!i.isFinite()) {
            return
        }
        val lowerBound = min ?: Double.NEGATIVE_INFINITY
        val upperBound = max ?: Double.POSITIVE_INFINITY
        val o = when {
            i < lowerBound -> lowerBound
            i > upperBound -> upperBound
            else -> i
        }
        setOutput(o)
    }
}
