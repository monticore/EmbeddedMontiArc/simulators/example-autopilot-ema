/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.DEFAULT_NUMERIC_COMPARATOR
import de.rwth.ryndin.modelling.autopilot.geometry.distanceTo
import de.rwth.ryndin.modelling.autopilot.geometry.plus
import de.rwth.ryndin.modelling.autopilot.toRadians
import de.rwth.ryndin.modelling.component.autopilot.behavior.DecideBehavior
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.VehicleState
import org.slf4j.LoggerFactory

class DecideBehaviorImplementation(
        turnAngleThreshold: Double,
        unfeasibleTurnAngleThreshold: Double,
        private val comparator: Comparator<Double> = DEFAULT_NUMERIC_COMPARATOR
) : DecideBehavior(turnAngleThreshold, unfeasibleTurnAngleThreshold) {

    companion object {
        private val LOG = LoggerFactory.getLogger(DecideBehaviorImplementation::class.java)
    }

    private lateinit var state: VehicleState
    private lateinit var currentPosition: Position
    private var currentVelocity = 0.0

    private val pathInterpolator = PathInterpolator(
            turnAngleThreshold.toRadians(),
            unfeasibleTurnAngleThreshold.toRadians(),
            comparator
    )

    private val velocityFunction = Distance2VelocityFunction(comparator)

    private var currentPath = listOf<Position>()
    private var behaviorInformation = listOf<BehaviorModel>()

    override fun execute() {
        state = vehicleState ?: return
        currentPosition = state.sensorData.position
        currentVelocity = state.sensorData.velocity
        if (isPathRebuilt == true) {
            rebuildPath()
        }
        trimPath()
        determineBehavior()
        LOG.info("planned route $currentPath")
    }


    private fun rebuildPath() {
        currentPath = emptyList()
        val (interpolated, behavior) = pathInterpolator.interpolate(path ?: emptyList())
        currentPath = interpolated
        behaviorInformation = behavior
    }

    private fun trimPath() {
        var closestPositionIndex = -1
        var closestPositionDistance = Double.POSITIVE_INFINITY
        currentPath.forEachIndexed { i, p ->
            val d = currentPosition.distanceTo(p)
            if (comparator.compare(d, closestPositionDistance) <= 0) {
                closestPositionIndex = i
                closestPositionDistance = d
            }
        }
        if (closestPositionIndex > 0) {
            while (closestPositionIndex < currentPath.size
                    && comparator.compare(currentPosition.distanceTo(currentPath[closestPositionIndex]), Distance2VelocityFunction.X1) < 0) {
                closestPositionIndex++
            }
            currentPath = currentPath.drop(closestPositionIndex)
            behaviorInformation = behaviorInformation.drop(closestPositionIndex)
        }
    }

    private fun determineBehavior() {
        if (currentPath.isEmpty()) {
            driveTo(currentPosition, 0.0, 0.0)
            return
        }
        val b = behaviorInformation[0]
        when (b) {
            is Turn -> {
                setDriveTurnDecisionVehicleState(state)
                setDriveTurnDecisionTurnMotionData(b.toStruct())
            }
            is DriveToPosition -> {
                val vMin = if (currentPath.size > 1) {
                    Distance2VelocityFunction.V1
                } else {
                    0.0
                }
                driveTo(b.targetPosition, vMin = vMin, vMax = Distance2VelocityFunction.V2)
            }
            is KeepDirection -> {
                val (i, straightDistance) = getKeepDirectionDistance()
                val v = calculateVelocity(i, straightDistance)
                keepDirection(b, v)
            }
        }
    }

    private fun driveTo(p: Position, vMin: Double? = null, vMax: Double? = null) {
        setDriveToDecisionCurrentPosition(currentPosition)
        setDriveToDecisionPosition(p)
        if (vMin != null) {
            setDriveToDecisionMinVelocity(vMin)
        }
        if (vMax != null) {
            setDriveToDecisionMaxVelocity(vMax)
        }
    }

    private fun getKeepDirectionDistance(): Pair<Int, Double> {
        var prev = currentPath[0]
        var i = 1
        var straightDistance = 0.0
        while (i < currentPath.size
                && comparator.compare(straightDistance, Distance2VelocityFunction.X4) < 0
                && behaviorInformation[i] is KeepDirection) {
            val next = currentPath[i]
            straightDistance += prev.distanceTo(next)
            prev = next
            i++
        }
        return Pair(i, straightDistance)
    }

    private fun calculateVelocity(i: Int, keepDirectionDistance: Double): Double {
        val defaultVelocity = velocityFunction(keepDirectionDistance)
        if (comparator.compare(keepDirectionDistance, Distance2VelocityFunction.X4) >= 0) {
            return defaultVelocity
        }
        if (i > currentPath.lastIndex) {
            return if (currentPath.size > 1) {
                maxOf(Distance2VelocityFunction.V1, defaultVelocity)
            } else {
                defaultVelocity
            }
        }
        val nextBehavior = behaviorInformation[i]
        if (nextBehavior is Turn) {
            return if (comparator.compare(keepDirectionDistance, Distance2VelocityFunction.X3) <= 0) {
                nextBehavior.velocity
            } else {
                maxOf(nextBehavior.velocity, defaultVelocity)
            }
        }
        if (nextBehavior is DriveToPosition) {
            val effectiveDistance = keepDirectionDistance + currentPath[i].distanceTo(nextBehavior.targetPosition)
            return maxOf(
                    Distance2VelocityFunction.V1,
                    velocityFunction(effectiveDistance)
            )
        }
        return defaultVelocity
    }

    private fun keepDirection(b: KeepDirection, velocity: Double) {
        val basePosition = currentPath[0]
        setDriveAlongDecisionCurrentPosition(currentPosition)
        setDriveAlongDecisionP1(basePosition)
        setDriveAlongDecisionP2(basePosition.plus(b.direction))
        setDriveAlongDecisionVelocity(velocity)
    }
}
