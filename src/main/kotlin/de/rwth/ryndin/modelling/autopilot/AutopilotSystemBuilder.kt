/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentBuilder
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.rwth.ryndin.modelling.autopilot.component.behavior.ApplyTurnWithFallbackToDriveAlongDirectionImplementation
import de.rwth.ryndin.modelling.autopilot.component.behavior.CalculateNextDrivingPositionImplementation
import de.rwth.ryndin.modelling.autopilot.component.behavior.CalculateVelocityImplementation
import de.rwth.ryndin.modelling.autopilot.component.behavior.DecideBehaviorImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.AbsImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.CalculateSignedDistanceToLineImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.CalculateVectorFromToImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.CalculateVectorLengthImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.ComposeControlCommandsImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.ConditionImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.EnsureBoundsDynamicImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.EnsureBoundsImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.FindClosestPathSegmentImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.IsNullImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.Node2IdImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.NormalizeVectorImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.NotImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.PIDImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.SelectFirstNonNullImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.SelectFirstNonNullVectorImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.SignedAngleBetweenImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.SumImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.VehicleState2CurrentDirectionImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.VehicleState2CurrentPositionImplementation
import de.rwth.ryndin.modelling.autopilot.component.common.VehicleState2CurrentVelocityImplementation
import de.rwth.ryndin.modelling.autopilot.component.motion.CalculatePidErrorImplementation
import de.rwth.ryndin.modelling.autopilot.component.motion.CalculatePidParametersImplementation
import de.rwth.ryndin.modelling.autopilot.component.motion.DecideEngineOrBrakesImplementation
import de.rwth.ryndin.modelling.autopilot.component.motion.SteeringAngleCorrectionImplementation
import de.rwth.ryndin.modelling.autopilot.component.perception.PerceptionImplementation
import de.rwth.ryndin.modelling.autopilot.component.planning.ActivateNavigatorImplementation
import de.rwth.ryndin.modelling.autopilot.component.planning.ChoosePathImplementation
import de.rwth.ryndin.modelling.autopilot.component.planning.DecidePathRecalculationImplementation
import de.rwth.ryndin.modelling.autopilot.component.planning.MapDataHolderImplementation
import de.rwth.ryndin.modelling.autopilot.component.planning.NavigatorImplementation
import de.rwth.ryndin.modelling.integration.ComposeActuationImplementation
import de.rwth.ryndin.modelling.integration.ComposeSensorDataImplementation
import de.rwth.ryndin.modelling.integration.SplitControlCommandsImplementation
import de.rwth.ryndin.modelling.meta.ToBeImplemented
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.MetaDataForAutopilot
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.ConfigurationParametersOfCalculateNextDrivingPosition
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.ConfigurationParametersOfDecideBehavior
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.ConfigurationParametersOfComposeControlCommands
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.ConfigurationParametersOfEnsureBounds
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.ConfigurationParametersOfCalculatePidError
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.ConfigurationParametersOfSteeringAngleCorrection
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.ConfigurationParametersOfDecidePathRecalculation

class AutopilotSystemBuilder(
        private val builder: ComponentBuilder = ComponentBuilder.create()
) : ComponentBuilder by builder {

    init {
        val factory = builder.factory
        factory.registerFactoryFunction(ToBeImplemented.idOfPerception, { PerceptionImplementation() })
        // common
        factory.registerFactoryFunction(ToBeImplemented.idOfEnsureBoundsDynamic, { EnsureBoundsDynamicImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfSelectFirstNonNull, { SelectFirstNonNullImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfSelectFirstNonNullVector, { SelectFirstNonNullVectorImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfCalculateSignedDistanceToLine, { CalculateSignedDistanceToLineImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfEnsureBounds, { params ->
            val min = params.parameters.single {
                it.name == ConfigurationParametersOfEnsureBounds.min
            }.value as Double
            val max = params.parameters.single {
                it.name == ConfigurationParametersOfEnsureBounds.max
            }.value as Double
            EnsureBoundsImplementation(min, max)
        })
        factory.registerFactoryFunction(ToBeImplemented.idOfPID, { PIDImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfNode2Id, { Node2IdImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfSignedAngleBetween, { SignedAngleBetweenImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfSum, { SumImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfCondition, { ConditionImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfAbs, { AbsImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfComposeControlCommands, { params ->
            val de = params.parameters.single {
                it.name == ConfigurationParametersOfComposeControlCommands.defaultEngine
            }.value as Double
            val ds = params.parameters.single {
                it.name == ConfigurationParametersOfComposeControlCommands.defaultSteering
            }.value as Double
            val db = params.parameters.single {
                it.name == ConfigurationParametersOfComposeControlCommands.defaultBrakes
            }.value as Double
            ComposeControlCommandsImplementation(de, ds, db)
        })
        factory.registerFactoryFunction(ToBeImplemented.idOfIsNull, { IsNullImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfNot, { NotImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfVehicleState2CurrentDirection, { VehicleState2CurrentDirectionImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfVehicleState2CurrentVelocity, { VehicleState2CurrentVelocityImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfVehicleState2CurrentPosition, { VehicleState2CurrentPositionImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfFindClosestPathSegment, { FindClosestPathSegmentImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfCalculateVectorFromTo, { CalculateVectorFromToImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfCalculateVectorLength, { CalculateVectorLengthImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfNormalizeVector, { NormalizeVectorImplementation() })
        // motion
        factory.registerFactoryFunction(ToBeImplemented.idOfCalculatePidError, { params ->
            val threshold = params.parameters.single {
                it.name == ConfigurationParametersOfCalculatePidError.velocityDifferenceThresholdForErrorLeap
            }.value as Double
            CalculatePidErrorImplementation(threshold)
        })
        factory.registerFactoryFunction(ToBeImplemented.idOfCalculatePidParameters, { CalculatePidParametersImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfDecideEngineOrBrakes, { DecideEngineOrBrakesImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfSteeringAngleCorrection, { params ->
            val msa = params.parameters.single {
                it.name == ConfigurationParametersOfSteeringAngleCorrection.maxSteeringAngle
            }.value as Double
            SteeringAngleCorrectionImplementation(msa)
        })
        // planning
        factory.registerFactoryFunction(ToBeImplemented.idOfNavigator, { NavigatorImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfChoosePath, { ChoosePathImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfMapDataHolder, { MapDataHolderImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfActivateNavigator, { ActivateNavigatorImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfDecidePathRecalculation, { params ->
            val stayOnTrajectoryThreshold = params.parameters.single {
                it.name == ConfigurationParametersOfDecidePathRecalculation.stayOnTrajectoryThreshold
            }.value as Double
            DecidePathRecalculationImplementation(stayOnTrajectoryThreshold)
        })
        // behavior
        factory.registerFactoryFunction(ToBeImplemented.idOfApplyTurnWithFallbackToDriveAlongDirection, { ApplyTurnWithFallbackToDriveAlongDirectionImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfCalculateVelocity, { CalculateVelocityImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfDecideBehavior, { params ->
            val tat = params.parameters.single {
                it.name == ConfigurationParametersOfDecideBehavior.turnAngleThreshold
            }.value as Double
            val utat = params.parameters.single {
                it.name == ConfigurationParametersOfDecideBehavior.unfeasibleTurnAngleThreshold
            }.value as Double
            DecideBehaviorImplementation(tat, utat)
        })
        factory.registerFactoryFunction(ToBeImplemented.idOfCalculateNextDrivingPosition, { params ->
            val la = params.parameters.single {
                it.name == ConfigurationParametersOfCalculateNextDrivingPosition.lookAhead
            }.value as Double
            CalculateNextDrivingPositionImplementation(la)
        })
        // integration
        factory.registerFactoryFunction(ToBeImplemented.idOfComposeActuation, { ComposeActuationImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfComposeSensorData, { ComposeSensorDataImplementation() })
        factory.registerFactoryFunction(ToBeImplemented.idOfSplitControlCommands, { SplitControlCommandsImplementation() })
    }

    val autopilotComponent: Component
        get() {
            return builder.buildComponent(
                    MetaDataForAutopilot,
                    ComponentCreationParameters.create()
            )
        }
}
