/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.geometry.normalize
import de.rwth.ryndin.modelling.autopilot.geometry.toModel
import de.rwth.ryndin.modelling.autopilot.geometry.toStruct
import de.rwth.ryndin.modelling.component.autopilot.common.NormalizeVector

class NormalizeVectorImplementation : NormalizeVector() {
    companion object {
        private val ALMOST_ZERO = 1e-5
    }

    override fun execute() {
        val v = vector ?: return
        val normalized = if (Math.abs(v.x) <= ALMOST_ZERO && Math.abs(v.y) <= ALMOST_ZERO) {
            v
        } else {
            v.toModel().normalize().toStruct()
        }
        setNormalized(normalized)
    }
}
