/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.VehicleState2CurrentVelocity

class VehicleState2CurrentVelocityImplementation : VehicleState2CurrentVelocity() {
    override fun execute() {
        setCurrentVelocity(vehicleState?.sensorData?.velocity ?: 0.0)
    }
}
