/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.geometry.signedDistanceToLine
import de.rwth.ryndin.modelling.component.autopilot.common.CalculateSignedDistanceToLine

class CalculateSignedDistanceToLineImplementation : CalculateSignedDistanceToLine() {
    override fun execute() {
        val givenPosition = position ?: return
        val p1 = pLine1 ?: return
        val p2 = pLine2 ?: return
        setSignedDistance(givenPosition.signedDistanceToLine(p1, p2))
    }
}
