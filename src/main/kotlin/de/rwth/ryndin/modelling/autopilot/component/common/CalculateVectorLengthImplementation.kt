/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.geometry.norm
import de.rwth.ryndin.modelling.autopilot.geometry.toModel
import de.rwth.ryndin.modelling.component.autopilot.common.CalculateVectorLength

class CalculateVectorLengthImplementation : CalculateVectorLength() {
    override fun execute() {
        val v = vector ?: return
        setLength(v.toModel().norm)
    }
}
