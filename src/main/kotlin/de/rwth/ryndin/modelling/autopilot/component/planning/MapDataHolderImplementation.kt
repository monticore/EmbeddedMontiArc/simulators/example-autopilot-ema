/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.autopilot.geometry.distanceTo
import de.rwth.ryndin.modelling.component.autopilot.planning.MapDataHolder
import de.rwth.ryndin.modelling.structure.autopilot.Edge
import de.rwth.ryndin.modelling.structure.autopilot.Node

class MapDataHolderImplementation : MapDataHolder() {

    private val nodeInformation = mutableMapOf<Long, Node>()
    private val edgeInformation = mutableMapOf<Pair<Long, Long>, Double>()

    override fun execute() {
        setIsMapDataChanged(false)
        updateEdges()
        updateNodes()
        calculateClosestPosition()
    }

    private fun updateEdges() {
        val edgesToUpdate = addOrUpdateEdges ?: emptyList()
        edgesToUpdate.forEach {
            edgeInformation.put(Pair(it.nodeFromId, it.nodeToId), it.distance)
        }
        var isEdgesUpdated = edgesToUpdate.isNotEmpty()
        val edgesToRemove = removeEdges ?: emptyList()
        edgesToRemove.forEach {
            val old = edgeInformation.remove(Pair(it.nodeFromId, it.nodeToId))
            isEdgesUpdated = isEdgesUpdated || old != null
        }
        if (isEdgesUpdated) {
            val edges = edgeInformation.map { (k, v) ->
                Edge(k.first, k.second, v)
            }
            setAllEdges(edges)
            setIsMapDataChanged(true)
        }
    }

    private fun updateNodes() {
        val nodesToAdd = addNodes ?: emptyList()
        nodesToAdd.forEach {
            nodeInformation.put(it.id, it)
        }
        var isNodesUpdated = nodesToAdd.isNotEmpty()
        val nodesToRemove = removeNodes ?: emptyList()
        nodesToRemove.forEach {
            val old = nodeInformation.remove(it.id)
            isNodesUpdated = isNodesUpdated || old != null
        }
        if (isNodesUpdated) {
            val nodes = nodeInformation.values.toList()
            setAllNodes(nodes)
            setIsMapDataChanged(true)
        }
    }

    private fun calculateClosestPosition() {
        val p0 = currentPosition ?: return
        val it = nodeInformation.values.iterator()
        if (!it.hasNext()) {
            return
        }
        var closest = it.next()
        var distance = p0.distanceTo(closest.position)
        while (it.hasNext()) {
            val p = it.next()
            val d = p0.distanceTo(p.position)
            if (d < distance) {
                distance = d
                closest = p
            }
        }
        setNodeClosestToCurrentPosition(closest)
        setDistanceToClosestNode(distance)
    }
}
