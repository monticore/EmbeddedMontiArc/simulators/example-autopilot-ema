/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.component.autopilot.common.SelectFirstNonNull

class SelectFirstNonNullImplementation : SelectFirstNonNull() {
    override fun execute() {
        when {
            input1 != null -> setOutput(input1!!)
            input2 != null -> setOutput(input2!!)
            input3 != null -> setOutput(input3!!)
        }
    }
}
