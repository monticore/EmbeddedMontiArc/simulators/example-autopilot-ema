/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.integration

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.integration.MetaDataForComposeActuation
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.integration.PortsOfComposeActuation
import de.rwth.ryndin.modelling.structure.autopilot.Actuation
import org.junit.Assert
import org.junit.Test

class ComposeActuationImplementationTest : ComponentTestBase(
        MetaDataForComposeActuation
) {

    @Test
    fun test1() {
        val o = execute(mapOf(
                PortsOfComposeActuation.inPort.engine to 1.0,
                PortsOfComposeActuation.inPort.steering to 2.0,
                PortsOfComposeActuation.inPort.brakes to 3.0
        ))
        Assert.assertEquals(
                Actuation(1.0, 2.0, 3.0),
                o[PortsOfComposeActuation.outPort.actuation]
        )
    }
}
