/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.integration

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.integration.MetaDataForSplitControlCommands
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.integration.PortsOfSplitControlCommands
import de.rwth.ryndin.modelling.structure.autopilot.Actuation
import de.rwth.ryndin.modelling.structure.autopilot.ControlCommands
import org.junit.Assert
import org.junit.Test

class SplitControlCommandsImplementationTest : ComponentTestBase(
        MetaDataForSplitControlCommands
) {

    @Test
    fun test1() {
        val o = execute(mapOf(
                PortsOfSplitControlCommands.inPort.controlCommands to ControlCommands(
                        Actuation(1.0, 2.0, 3.0)
                )
        ))
        Assert.assertEquals(
                3.0,
                o[PortsOfSplitControlCommands.outPort.brakes] as Double,
                1e-2
        )
        Assert.assertEquals(
                1.0,
                o[PortsOfSplitControlCommands.outPort.engine] as Double,
                1e-2
        )
        Assert.assertEquals(
                2.0,
                o[PortsOfSplitControlCommands.outPort.steering] as Double,
                1e-2
        )
    }
}
