/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.integration

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.integration.MetaDataForComposeSensorData
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.integration.PortsOfComposeSensorData
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.SensorData
import org.junit.Assert
import org.junit.Test

class ComposeSensorDataImplementationTest : ComponentTestBase(
        MetaDataForComposeSensorData
) {

    @Test
    fun test1() {
        val o = execute(mapOf(
                PortsOfComposeSensorData.inPort.positionX to 1.0,
                PortsOfComposeSensorData.inPort.positionY to 2.0,
                PortsOfComposeSensorData.inPort.compass to 3.0,
                PortsOfComposeSensorData.inPort.currentVelocity to 4.0
        ))
        Assert.assertEquals(
                SensorData(4.0, 3.0, Position(1.0, 2.0)),
                o[PortsOfComposeSensorData.outPort.sensorData]
        )
    }
}
