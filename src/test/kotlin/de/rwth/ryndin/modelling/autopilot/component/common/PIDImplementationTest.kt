/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.MetaDataForPID
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.PortsOfPID
import org.junit.Assert
import org.junit.Test

class PIDImplementationTest : ComponentTestBase(
        MetaDataForPID
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertEquals(0.0, o[PortsOfPID.outPort.control] as Double, 1e-2)
    }

    @Test
    fun test2() {
        test(1.0, 0.0, 0.0, 0.0, 10.0, 10.0)
    }

    @Test
    fun test3() {
        test(2.0, 0.0, 0.0, 0.0, -2.0, -4.0)
    }

    @Test
    fun test4() {
        test(1.0, 0.0, 0.0, 0.0, 9.0, 9.0)
        test(2.0, 0.0, 0.0, 0.0, -3.0, -6.0)
        test(2.0, 3.0, 5.0, 0.01, 0.5, 19.91)
    }

    private fun test(p: Double, i: Double, d: Double, decay: Double, error: Double, control: Double) {
        val o = execute(mapOf(
                PortsOfPID.inPort.paramP to p,
                PortsOfPID.inPort.paramI to i,
                PortsOfPID.inPort.paramD to d,
                PortsOfPID.inPort.paramDecayCoefficient to decay,
                PortsOfPID.inPort.error to error
        ))
        Assert.assertEquals(control, o[PortsOfPID.outPort.control] as Double, 1e-2)
    }
}
