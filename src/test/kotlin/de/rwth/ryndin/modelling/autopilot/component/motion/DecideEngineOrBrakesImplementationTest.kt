/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.motion

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.MetaDataForDecideEngineOrBrakes
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.PortsOfDecideEngineOrBrakes
import org.junit.Assert
import org.junit.Test

class DecideEngineOrBrakesImplementationTest : ComponentTestBase(
        MetaDataForDecideEngineOrBrakes
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertEquals(0.0, o[PortsOfDecideEngineOrBrakes.outPort.engine] as Double, 1e-2)
        Assert.assertEquals(0.0, o[PortsOfDecideEngineOrBrakes.outPort.brakes] as Double, 1e-2)
    }

    @Test
    fun test2() {
        testExpectedOutputs(1.0, 5.0, 5.0, 0.0)
    }

    @Test
    fun test3() {
        testExpectedOutputs(-10.0, 20.0, 0.0, 20.0)
    }

    private fun testExpectedOutputs(error: Double, control: Double, expectedEngine: Double, expectedBrakes: Double) {
        val inputs = mapOf(
                PortsOfDecideEngineOrBrakes.inPort.error to error,
                PortsOfDecideEngineOrBrakes.inPort.controlSignal to control
        )
        val o = execute(inputs)
        val engine = o[PortsOfDecideEngineOrBrakes.outPort.engine] as Double
        val brakes = o[PortsOfDecideEngineOrBrakes.outPort.brakes] as Double
        Assert.assertEquals(expectedEngine, engine, 1e-2)
        Assert.assertEquals(expectedBrakes, brakes, 1e-2)
    }
}
