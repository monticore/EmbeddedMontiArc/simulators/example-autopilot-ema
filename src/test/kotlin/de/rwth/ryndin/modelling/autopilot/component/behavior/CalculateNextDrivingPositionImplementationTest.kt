/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter
import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.ConfigurationParametersOfCalculateNextDrivingPosition
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.MetaDataForCalculateNextDrivingPosition
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.PortsOfCalculateNextDrivingPosition
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.junit.Assert
import org.junit.Test

class CalculateNextDrivingPositionImplementationTest : ComponentTestBase(
        MetaDataForCalculateNextDrivingPosition,
        ComponentCreationParameters.create(listOf(), listOf(Parameter.create(ConfigurationParametersOfCalculateNextDrivingPosition.lookAhead, 10.0)))
) {

    @Test
    fun test1() {
        val currentPosition = Position(1.5, 0.5)
        val inputs = mapOf(
                PortsOfCalculateNextDrivingPosition.inPort.currentPosition to currentPosition,
                PortsOfCalculateNextDrivingPosition.inPort.p1 to Position(1.0, 3.0),
                PortsOfCalculateNextDrivingPosition.inPort.p2 to Position(5.0, 1.0)
        )
        val outputs = execute(inputs)
        val next = outputs[PortsOfCalculateNextDrivingPosition.outPort.next] as Position
        Assert.assertEquals(11.344, next.x, 1e-2)
        Assert.assertEquals(-2.172, next.y, 1e-2)
    }

    @Test
    fun test2() {
        val currentPosition = Position(1.5, 0.5)
        val inputs = mapOf(
                PortsOfCalculateNextDrivingPosition.inPort.currentPosition to currentPosition,
                PortsOfCalculateNextDrivingPosition.inPort.p1 to Position(1.0, 3.0)
        )
        val outputs = execute(inputs)
        Assert.assertNull(outputs[PortsOfCalculateNextDrivingPosition.outPort.next])
    }
}
