/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.MetaDataForMapDataHolder
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.PortsOfMapDataHolder
import de.rwth.ryndin.modelling.structure.autopilot.Edge
import de.rwth.ryndin.modelling.structure.autopilot.Node
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.junit.Assert
import org.junit.Test

class MapDataHolderImplementationTest : ComponentTestBase(
        MetaDataForMapDataHolder
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertNull(o[PortsOfMapDataHolder.outPort.allEdges])
        Assert.assertNull(o[PortsOfMapDataHolder.outPort.allNodes])
        Assert.assertNull(o[PortsOfMapDataHolder.outPort.distanceToClosestNode])
        Assert.assertFalse(o[PortsOfMapDataHolder.outPort.isMapDataChanged] as Boolean)
        Assert.assertNull(o[PortsOfMapDataHolder.outPort.nodeClosestToCurrentPosition])
    }

    @Test
    fun test2() {
        val addNodes = listOf(
                Node(1L, Position(1.0, 1.0)),
                Node(2L, Position(2.0, 2.0))
        )
        val addOrUpdateEdges = listOf(
                Edge(1L, 2L, 10.0)
        )
        val cp = Position(1.1, 1.1)
        val inputs = mapOf(
                PortsOfMapDataHolder.inPort.addNodes to addNodes,
                PortsOfMapDataHolder.inPort.addOrUpdateEdges to addOrUpdateEdges,
                PortsOfMapDataHolder.inPort.currentPosition to cp
        )
        val o = execute(inputs)
        Assert.assertEquals(addOrUpdateEdges, o[PortsOfMapDataHolder.outPort.allEdges])
        Assert.assertEquals(addNodes, o[PortsOfMapDataHolder.outPort.allNodes])
        Assert.assertEquals(0.141, o[PortsOfMapDataHolder.outPort.distanceToClosestNode] as Double, 1e-3)
        Assert.assertTrue(o[PortsOfMapDataHolder.outPort.isMapDataChanged] as Boolean)
        Assert.assertEquals(Node(1L, Position(1.0, 1.0)), o[PortsOfMapDataHolder.outPort.nodeClosestToCurrentPosition])
    }

    @Test
    fun test3() {
        val addNodes = listOf(
                Node(1L, Position(1.0, 1.0)),
                Node(2L, Position(2.0, 2.0)),
                Node(3L, Position(3.0, 3.0)),
                Node(4L, Position(4.0, 4.0)),
                Node(5L, Position(5.0, 5.0))
        )
        val addOrUpdateEdges = listOf(
                Edge(1L, 2L, 10.0),
                Edge(2L, 3L, 20.0),
                Edge(3L, 4L, 30.0),
                Edge(4L, 5L, 40.0),
                Edge(5L, 1L, 50.0)
        )
        val cp = Position(4.1, 4.1)
        val inputs = mapOf(
                PortsOfMapDataHolder.inPort.addNodes to addNodes,
                PortsOfMapDataHolder.inPort.addOrUpdateEdges to addOrUpdateEdges,
                PortsOfMapDataHolder.inPort.currentPosition to cp
        )
        val o = execute(inputs)
        Assert.assertEquals(addOrUpdateEdges, o[PortsOfMapDataHolder.outPort.allEdges])
        Assert.assertEquals(addNodes, o[PortsOfMapDataHolder.outPort.allNodes])
        Assert.assertEquals(0.141, o[PortsOfMapDataHolder.outPort.distanceToClosestNode] as Double, 1e-3)
        Assert.assertTrue(o[PortsOfMapDataHolder.outPort.isMapDataChanged] as Boolean)
        Assert.assertEquals(Node(4L, Position(4.0, 4.0)), o[PortsOfMapDataHolder.outPort.nodeClosestToCurrentPosition])
    }

    @Test
    fun test4() {
        val addNodes = listOf(
                Node(1L, Position(1.0, 1.0)),
                Node(2L, Position(2.0, 2.0)),
                Node(3L, Position(3.0, 3.0)),
                Node(4L, Position(4.0, 4.0)),
                Node(5L, Position(5.0, 5.0))
        )
        val addOrUpdateEdges = listOf(
                Edge(1L, 2L, 10.0),
                Edge(2L, 3L, 20.0),
                Edge(3L, 4L, 30.0),
                Edge(4L, 5L, 40.0),
                Edge(5L, 1L, 50.0)
        )
        val cp = Position(3.1, 3.1)
        execute(mapOf(
                PortsOfMapDataHolder.inPort.addNodes to addNodes,
                PortsOfMapDataHolder.inPort.addOrUpdateEdges to addOrUpdateEdges,
                PortsOfMapDataHolder.inPort.currentPosition to cp
        ))
        val removeNodes = listOf(
                Node(1L, Position(1.0, 1.0)),
                Node(2L, Position(2.0, 2.0)),
                Node(3L, Position(3.0, 3.0))
        )
        val removeEdges = listOf(
                Edge(5L, 1L, 0.0)
        )
        val o = execute(mapOf(
                PortsOfMapDataHolder.inPort.removeNodes to removeNodes,
                PortsOfMapDataHolder.inPort.removeEdges to removeEdges,
                PortsOfMapDataHolder.inPort.currentPosition to cp
        ))
        Assert.assertEquals(
                listOf(
                        Edge(1L, 2L, 10.0),
                        Edge(2L, 3L, 20.0),
                        Edge(3L, 4L, 30.0),
                        Edge(4L, 5L, 40.0)
                ),
                o[PortsOfMapDataHolder.outPort.allEdges]
        )
        Assert.assertEquals(
                listOf(
                        Node(4L, Position(4.0, 4.0)),
                        Node(5L, Position(5.0, 5.0))
                ),
                o[PortsOfMapDataHolder.outPort.allNodes]
        )
        Assert.assertEquals(1.272, o[PortsOfMapDataHolder.outPort.distanceToClosestNode] as Double, 1e-3)
        Assert.assertTrue(o[PortsOfMapDataHolder.outPort.isMapDataChanged] as Boolean)
        Assert.assertEquals(Node(4L, Position(4.0, 4.0)), o[PortsOfMapDataHolder.outPort.nodeClosestToCurrentPosition])
    }
}
