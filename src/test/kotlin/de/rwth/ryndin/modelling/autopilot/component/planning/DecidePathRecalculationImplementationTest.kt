/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter
import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.ConfigurationParametersOfDecidePathRecalculation
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.MetaDataForDecidePathRecalculation
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.PortsOfDecidePathRecalculation
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.junit.Assert
import org.junit.Test

class DecidePathRecalculationImplementationTest : ComponentTestBase(
        MetaDataForDecidePathRecalculation,
        ComponentCreationParameters.create(listOf(), listOf(
                Parameter.create(ConfigurationParametersOfDecidePathRecalculation.stayOnTrajectoryThreshold, 3.0)
        ))
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertNull(o[PortsOfDecidePathRecalculation.outPort.trimmedPath])
        Assert.assertTrue(o[PortsOfDecidePathRecalculation.outPort.isRecalculatePath] as Boolean)
    }

    @Test
    fun test2() {
        val d = 1.0
        val index = 1L
        val path = listOf(
                Position(0.0, 0.0),
                Position(1.0, 0.0)
        )
        val dPrev = 5.0
        val o = execute(mapOf(
                PortsOfDecidePathRecalculation.inPort.distanceToPath to d,
                PortsOfDecidePathRecalculation.inPort.indexOfClosestSegment to index,
                PortsOfDecidePathRecalculation.inPort.path to path,
                PortsOfDecidePathRecalculation.inPort.previousDistanceToPath to dPrev
        ))
        val isRecalculatePath = o[PortsOfDecidePathRecalculation.outPort.isRecalculatePath] as Boolean
        val trimmedPath = o[PortsOfDecidePathRecalculation.outPort.trimmedPath] as List<Position>
        Assert.assertFalse(isRecalculatePath)
        Assert.assertEquals(1, trimmedPath.size)
        Assert.assertEquals(Position(1.0, 0.0), trimmedPath[0])
    }

    @Test
    fun test3() {
        val d = 5.0
        val index = 1L
        val path = listOf(
                Position(0.0, 0.0),
                Position(1.0, 0.0)
        )
        val dPrev = 3.0
        val o = execute(mapOf(
                PortsOfDecidePathRecalculation.inPort.distanceToPath to d,
                PortsOfDecidePathRecalculation.inPort.indexOfClosestSegment to index,
                PortsOfDecidePathRecalculation.inPort.path to path,
                PortsOfDecidePathRecalculation.inPort.previousDistanceToPath to dPrev
        ))
        val isRecalculatePath = o[PortsOfDecidePathRecalculation.outPort.isRecalculatePath] as Boolean
        val trimmedPath = o[PortsOfDecidePathRecalculation.outPort.trimmedPath]
        Assert.assertTrue(isRecalculatePath)
        Assert.assertNull(trimmedPath)
    }
}
