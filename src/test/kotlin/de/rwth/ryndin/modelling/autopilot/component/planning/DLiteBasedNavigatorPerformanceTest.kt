/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import com.fasterxml.jackson.databind.ObjectMapper
import de.rwth.ryndin.modelling.structure.autopilot.Node
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test
import java.io.File


internal class DLiteBasedNavigatorPerformanceTest {

    companion object {
        lateinit var nodes: Map<Id, Node>
        lateinit var edges: Map<Pair<Id, Id>, Double>
        lateinit var heuristic: HeuristicFunction

        @BeforeClass
        @JvmStatic
        fun setupSpec() {
            val mapper = ObjectMapper()
            val classLoader = javaClass.classLoader
            val nodesData = mapper.readValue<List<NodeDTO>>(
                    File(classLoader.getResource("map/nodes.json").file),
                    mapper.typeFactory.constructCollectionType(List::class.java, NodeDTO::class.java)
            )
            val edgesData = mapper.readValue<List<EdgeDTO>>(
                    File(classLoader.getResource("map/edges.json").file),
                    mapper.typeFactory.constructCollectionType(List::class.java, EdgeDTO::class.java)
            )
            nodes = nodesData.map { it.id to Node(it.id, Position(it.x, it.y)) }.toMap()
            edges = edgesData.map { Pair(it.fromNodeId, it.toNodeId) to it.cost }.toMap()
            heuristic = EuclideanHeuristic(nodes)
        }
    }

    lateinit var navigator: Navigator
    lateinit var referenceNavigator: Navigator

    @Test
    @Ignore("long running test")
    fun test() {
        setup(1968783617, 60009495)
        val time1 = benchmark(navigator)
        val time2 = benchmark(referenceNavigator)
        // results for 10000 cycles:
        // time1 = 4.6151
        // time2 = 13.9276
        // results for 1000 cycles:
        // time1 = 6.939
        // time2 = 16.914
        Assert.assertTrue(time1 < time2)
    }

    private fun setup(initialStartNodeId: Id, goalNodeId: Id) {
        navigator = DLiteBasedNavigator(edges, initialStartNodeId, goalNodeId, heuristic)
        referenceNavigator = DijkstraBasedNavigator(edges, goalNodeId)
    }

    private fun benchmark(nav: Navigator): Double {
        val controlNodes = listOf(
                1968783617L,
                298892618L,
                35855882L,
                60009522L,
                60009513L,
                1709993640L,
                616794425L
        )
        val edgesToModify = listOf(
                Pair(60775807L, 828484200L)
                , Pair(982622428L, 1215161067L)
                , Pair(35856142L, 950034512L)
                , Pair(60009498L, 1976431761L)
                , Pair(36951777L, 1710273553L)
        )
        val edgesToRemove = listOf(
                Pair(1396094769L, 60009495L)
                , Pair(60775806L, 923950490L)
                , Pair(1228418855L, 60009501L)
                , Pair(35856140L, 1603590243L)

        )
        val start = System.currentTimeMillis()
        val numberOfCycles = 1_000
        (1..numberOfCycles).forEach {
            controlNodes.forEach {
                nav.getShortestPathFrom(it)
            }
            edgesToModify.forEach {
                nav.addOrUpdateEdge(it.first, it.second, 1e5)
                controlNodes.forEach {
                    nav.getShortestPathFrom(it)
                }
            }
            // restore
            edgesToModify.forEach {
                nav.addOrUpdateEdge(it.first, it.second, edges[it]!!)
            }
            controlNodes.forEach {
                nav.getShortestPathFrom(it)
            }
            edgesToRemove.forEach {
                nav.removeEdge(it.first, it.second)
                controlNodes.forEach {
                    nav.getShortestPathFrom(it)
                }
            }
            // restore
            edgesToRemove.forEach {
                nav.addOrUpdateEdge(it.first, it.second, edges[it]!!)
            }
        }
        val diff = System.currentTimeMillis() - start
        return diff.toDouble() / numberOfCycles.toDouble()
    }
}

internal data class NodeDTO(
        var id: Long = -1,
        var x: Double = Double.NaN,
        var y: Double = Double.NaN
)

internal data class EdgeDTO(
        var fromNodeId: Long = -1,
        var toNodeId: Long = -1,
        var cost: Double = Double.NaN
)
