/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.MetaDataForBehaviorGeneration
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.PortsOfBehaviorGeneration
import de.rwth.ryndin.modelling.structure.autopilot.Actuation
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.SensorData
import de.rwth.ryndin.modelling.structure.autopilot.Vector
import de.rwth.ryndin.modelling.structure.autopilot.VehicleState
import org.junit.Assert
import org.junit.Test

class BehaviorGenerationComponentTest : ComponentTestBase(
        MetaDataForBehaviorGeneration
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertNull(o[PortsOfBehaviorGeneration.outPort.desiredDirection])
        Assert.assertNull(o[PortsOfBehaviorGeneration.outPort.desiredSteeringAngle])
        Assert.assertNull(o[PortsOfBehaviorGeneration.outPort.desiredVelocity])
        Assert.assertNull(o[PortsOfBehaviorGeneration.outPort.signedDistanceToTrajectory])
    }

    @Test
    fun test2() {
        val cp = Position(0.0, 0.0)
        val vehicleState = VehicleState(
                Actuation(0.0, 0.0, 0.0),
                SensorData(2.0, 0.0, cp)
        )
        val path = listOf(
                Position(1.0, 1.0)
        )
        val isPathRebuilt = true
        val o = execute(mapOf(
                PortsOfBehaviorGeneration.inPort.vehicleState to vehicleState,
                PortsOfBehaviorGeneration.inPort.path to path,
                PortsOfBehaviorGeneration.inPort.isPathRebuilt to isPathRebuilt
        ))
        val desiredDirection = o[PortsOfBehaviorGeneration.outPort.desiredDirection] as? Vector
        val desiredSteeringAngle = o[PortsOfBehaviorGeneration.outPort.desiredSteeringAngle] as? Double
        val desiredVelocity = o[PortsOfBehaviorGeneration.outPort.desiredVelocity] as? Double
        val signedDistanceToTrajectory = o[PortsOfBehaviorGeneration.outPort.signedDistanceToTrajectory] as? Double
        Assert.assertNotNull(desiredDirection)
        Assert.assertNull(desiredSteeringAngle)
        Assert.assertNotNull(desiredVelocity)
        Assert.assertNull(signedDistanceToTrajectory)
        Assert.assertEquals(0.707, desiredDirection!!.x, 1e-2)
        Assert.assertEquals(0.707, desiredDirection!!.y, 1e-2)
        Assert.assertEquals(1.12, desiredVelocity!!, 1e-2)
    }

    @Test
    fun test3() {
        val cp = Position(-1.0, 0.0)
        val vehicleState = VehicleState(
                Actuation(0.0, 0.0, 0.0),
                SensorData(2.0, -Math.PI / 2, cp)
        )
        val path = listOf(
                Position(0.0, 0.0),
                Position(1.0, 0.0),
                Position(2.0, 0.0)
        )
        val isPathRebuilt = true
        val o = execute(mapOf(
                PortsOfBehaviorGeneration.inPort.vehicleState to vehicleState,
                PortsOfBehaviorGeneration.inPort.path to path,
                PortsOfBehaviorGeneration.inPort.isPathRebuilt to isPathRebuilt
        ))
        val desiredDirection = o[PortsOfBehaviorGeneration.outPort.desiredDirection] as? Vector
        val desiredSteeringAngle = o[PortsOfBehaviorGeneration.outPort.desiredSteeringAngle] as? Double
        val desiredVelocity = o[PortsOfBehaviorGeneration.outPort.desiredVelocity] as? Double
        val signedDistanceToTrajectory = o[PortsOfBehaviorGeneration.outPort.signedDistanceToTrajectory] as? Double
        Assert.assertNotNull(desiredDirection)
        Assert.assertNull(desiredSteeringAngle)
        Assert.assertNotNull(desiredVelocity)
        Assert.assertNotNull(signedDistanceToTrajectory)
        Assert.assertEquals(1.0, desiredDirection!!.x, 1e-2)
        Assert.assertEquals(0.0, desiredDirection!!.y, 1e-2)
        Assert.assertEquals(1.0, desiredVelocity!!, 1e-2)
        Assert.assertEquals(0.0, signedDistanceToTrajectory!!, 1e-2)
    }
}
