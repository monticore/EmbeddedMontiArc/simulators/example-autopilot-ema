/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.MetaDataForChoosePath
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.PortsOfChoosePath
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.junit.Assert
import org.junit.Test

class ChoosePathImplementationTest : ComponentTestBase(
        MetaDataForChoosePath
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertNull(o[PortsOfChoosePath.outPort.isPathRebuilt])
        Assert.assertNull(o[PortsOfChoosePath.outPort.resultingPath])
    }

    @Test
    fun test2() {
        val path = listOf(Position(0.0, 0.0), Position(1.0, 1.0))
        val o = execute(mapOf(
                PortsOfChoosePath.inPort.path to path
        ))
        val resultingPath = o[PortsOfChoosePath.outPort.resultingPath]
        val isPathRebuilt = o[PortsOfChoosePath.outPort.isPathRebuilt] as Boolean
        Assert.assertEquals(path, resultingPath)
        Assert.assertFalse(isPathRebuilt)
    }

    @Test
    fun test3() {
        val path = listOf(Position(0.0, 0.0), Position(1.0, 1.0))
        val o = execute(mapOf(
                PortsOfChoosePath.inPort.recalculatedPath to path
        ))
        val resultingPath = o[PortsOfChoosePath.outPort.resultingPath]
        val isPathRebuilt = o[PortsOfChoosePath.outPort.isPathRebuilt] as Boolean
        Assert.assertEquals(path, resultingPath)
        Assert.assertTrue(isPathRebuilt)
    }
}
