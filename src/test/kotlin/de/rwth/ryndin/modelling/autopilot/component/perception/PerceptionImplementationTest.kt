/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.perception

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.perception.MetaDataForPerception
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.perception.PortsOfPerception
import de.rwth.ryndin.modelling.structure.autopilot.Actuation
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.SensorData
import de.rwth.ryndin.modelling.structure.autopilot.VehicleState
import org.junit.Assert
import org.junit.Test

class PerceptionImplementationTest : ComponentTestBase(
        MetaDataForPerception
) {

    @Test
    fun test1() {
        val a = Actuation(1.0, 2.0, 3.0)
        val sd = SensorData(
                1.0,
                2.0,
                Position(3.0, 4.0)
        )
        val o = execute(mapOf(
                PortsOfPerception.inPort.actuation to a,
                PortsOfPerception.inPort.sensorData to sd
        ))
        val expected = VehicleState(a, sd)
        Assert.assertEquals(expected, o[PortsOfPerception.outPort.vehicleState])
    }
}
