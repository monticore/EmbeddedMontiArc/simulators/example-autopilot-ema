/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.MetaDataForActivateNavigator
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.PortsOfActivateNavigator
import org.junit.Assert
import org.junit.Test

class ActivateNavigatorImplementationTest : ComponentTestBase(
        MetaDataForActivateNavigator
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertNull(o[PortsOfActivateNavigator.outPort.outputStartNodeId])
    }

    @Test
    fun test2() {
        testExpectedOutputs(1, false, false, null)
    }

    @Test
    fun test3() {
        testExpectedOutputs(2, false, true, 2)
    }

    @Test
    fun test4() {
        testExpectedOutputs(3, true, false, 3)
    }

    @Test
    fun test5() {
        testExpectedOutputs(4, true, true, 4)
    }

    private fun testExpectedOutputs(startNodeId: Long, isMapDataChanged: Boolean, isRecalculatePath: Boolean, expected: Long?) {
        val inputs = mapOf(
                PortsOfActivateNavigator.inPort.inputStartNodeId to startNodeId,
                PortsOfActivateNavigator.inPort.isMapDataChanged to isMapDataChanged,
                PortsOfActivateNavigator.inPort.isRecalculatePath to isRecalculatePath
        )
        val o = execute(inputs)
        val id = o[PortsOfActivateNavigator.outPort.outputStartNodeId] as? Long
        if (expected != null) {
            Assert.assertNotNull(id)
            Assert.assertEquals(expected, id)
        } else {
            Assert.assertNull(id)
        }
    }
}
