/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.motion

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.MetaDataForMotionPlanning
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.PortsOfMotionPlanning
import de.rwth.ryndin.modelling.structure.autopilot.Actuation
import de.rwth.ryndin.modelling.structure.autopilot.ControlCommands
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.SensorData
import de.rwth.ryndin.modelling.structure.autopilot.Vector
import de.rwth.ryndin.modelling.structure.autopilot.VehicleState
import org.junit.Assert
import org.junit.Test

class MotionPlanningComponentTest : ComponentTestBase(
        MetaDataForMotionPlanning
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertNotNull(o[PortsOfMotionPlanning.outPort.controlCommands])
    }

    @Test
    fun test2() {
        val cp = Position(0.0, 0.0)
        val vehicleState = VehicleState(
                Actuation(1.5, 0.0, 0.0),
                SensorData(3.0, 0.0, cp)
        )
        val signedDistanceToTrajectory = 1
        val desiredDirection = Vector(1.0, 1.0)
        val desiredSteeringAngle = 0.0
        val desiredVelocity = 2.5
        val o = execute(mapOf(
                PortsOfMotionPlanning.inPort.desiredDirection to desiredDirection,
                PortsOfMotionPlanning.inPort.vehicleState to vehicleState,
                PortsOfMotionPlanning.inPort.signedDistanceToTrajectory to signedDistanceToTrajectory,
                PortsOfMotionPlanning.inPort.desiredSteeringAngle to desiredSteeringAngle,
                PortsOfMotionPlanning.inPort.desiredVelocity to desiredVelocity
        ))
        val cc = o[PortsOfMotionPlanning.outPort.controlCommands] as ControlCommands
        val a = cc.actuation
        Assert.assertTrue(a.engine <= 0.1)
        Assert.assertTrue(a.steering <= 0.1)
        Assert.assertTrue(a.brakes > 0.1)
    }

    @Test
    fun test3() {
        val cp = Position(0.0, 0.0)
        val vehicleState = VehicleState(
                Actuation(1.5, 0.0, 0.0),
                SensorData(3.0, 0.0, cp)
        )
        val signedDistanceToTrajectory = 1
        val desiredDirection = Vector(1.0, 1.0)
        val desiredSteeringAngle = 0.0
        val desiredVelocity = 10.0
        val o = execute(mapOf(
                PortsOfMotionPlanning.inPort.desiredDirection to desiredDirection,
                PortsOfMotionPlanning.inPort.vehicleState to vehicleState,
                PortsOfMotionPlanning.inPort.signedDistanceToTrajectory to signedDistanceToTrajectory,
                PortsOfMotionPlanning.inPort.desiredSteeringAngle to desiredSteeringAngle,
                PortsOfMotionPlanning.inPort.desiredVelocity to desiredVelocity
        ))
        val cc = o[PortsOfMotionPlanning.outPort.controlCommands] as ControlCommands
        val a = cc.actuation
        Assert.assertTrue(a.engine > 0.1)
        Assert.assertTrue(a.steering <= 0.1)
        Assert.assertTrue(a.brakes <= 0.1)
    }
}
