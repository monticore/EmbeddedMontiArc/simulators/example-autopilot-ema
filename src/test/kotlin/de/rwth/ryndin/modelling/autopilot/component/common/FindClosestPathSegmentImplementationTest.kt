/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.MetaDataForFindClosestPathSegment
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.PortsOfFindClosestPathSegment
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.junit.Assert
import org.junit.Test

class FindClosestPathSegmentImplementationTest : ComponentTestBase(
        MetaDataForFindClosestPathSegment
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertNull(o[PortsOfFindClosestPathSegment.outPort.distance])
        Assert.assertNull(o[PortsOfFindClosestPathSegment.outPort.index])
    }

    @Test
    fun test2() {
        val path = listOf(
                Position(1.0, 1.0)
        )
        val position = Position(0.0, 0.0)
        val o = execute(mapOf(
                PortsOfFindClosestPathSegment.inPort.path to path,
                PortsOfFindClosestPathSegment.inPort.position to position
        ))
        val distance = o[PortsOfFindClosestPathSegment.outPort.distance] as Double
        val index = o[PortsOfFindClosestPathSegment.outPort.index] as Long
        Assert.assertEquals(1.414, distance, 1e-3)
        Assert.assertEquals(0L, index)
    }

    @Test
    fun test3() {
        val path = listOf(
                Position(1.0, 1.0),
                Position(2.0, 2.0),
                Position(3.0, 3.0)
        )
        val position = Position(2.1, 2.1)
        val o = execute(mapOf(
                PortsOfFindClosestPathSegment.inPort.path to path,
                PortsOfFindClosestPathSegment.inPort.position to position
        ))
        val distance = o[PortsOfFindClosestPathSegment.outPort.distance] as Double
        val index = o[PortsOfFindClosestPathSegment.outPort.index] as Long
        Assert.assertEquals(0.0, distance, 1e-3)
        Assert.assertEquals(1L, index)
    }

    @Test
    fun test4() {
        val path = listOf(
                Position(1.0, 1.0),
                Position(2.0, 2.0),
                Position(3.0, 3.0)
        )
        val position = Position(3.2, 3.0)
        val o = execute(mapOf(
                PortsOfFindClosestPathSegment.inPort.path to path,
                PortsOfFindClosestPathSegment.inPort.position to position
        ))
        val distance = o[PortsOfFindClosestPathSegment.outPort.distance] as Double
        val index = o[PortsOfFindClosestPathSegment.outPort.index] as Long
        Assert.assertEquals(0.2, distance, 1e-3)
        Assert.assertEquals(1L, index)
    }
}
