/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component

import de.monticore.lang.monticar.ema2kt.runtime.api.Component
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentMetaData
import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentSystem
import de.monticore.lang.monticar.ema2kt.runtime.api.clearAllInPorts
import de.monticore.lang.monticar.ema2kt.runtime.api.clearAllOutPorts
import de.monticore.lang.monticar.ema2kt.runtime.api.toComponentSystem
import de.rwth.ryndin.modelling.autopilot.AutopilotSystemBuilder
import org.junit.Before

abstract class ComponentTestBase(
        protected val meta: ComponentMetaData,
        protected val defaultBuildParams: ComponentCreationParameters = ComponentCreationParameters.create()
) {

    protected val autopilotSystemBuilder = AutopilotSystemBuilder()
    protected lateinit var component: Component
    protected lateinit var componentSystem: ComponentSystem

    @Before
    open fun setup() {
        buildComponent()
    }

    protected fun buildComponent() {
        buildComponent(defaultBuildParams)
    }

    protected fun buildComponent(params: ComponentCreationParameters) {
        component = autopilotSystemBuilder.buildComponent(meta, params)
        componentSystem = component.toComponentSystem()
    }

    protected fun execute(inputs: Map<String, Any?>): Map<String, Any?> {
        val outputs = componentSystem.execute(inputs)
        component.clearAllInPorts()
        component.clearAllOutPorts()
        return outputs
    }
}
