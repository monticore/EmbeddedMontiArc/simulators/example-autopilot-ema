/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.motion

import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter
import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.ConfigurationParametersOfSteeringAngleCorrection
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.MetaDataForSteeringAngleCorrection
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.PortsOfSteeringAngleCorrection
import org.junit.Assert
import org.junit.Test

class SteeringAngleCorrectionImplementationTest : ComponentTestBase(
        MetaDataForSteeringAngleCorrection,
        ComponentCreationParameters.create(listOf(), listOf(
                Parameter.create(ConfigurationParametersOfSteeringAngleCorrection.maxSteeringAngle, 0.785)
        ))
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertEquals(0.0, o[PortsOfSteeringAngleCorrection.outPort.steeringAngleCorrection] as Double, 1e-2)
    }

    @Test
    fun test2() {
        testExpectedOutputs(0.0, 0.0)
    }

    @Test
    fun test3() {
        testExpectedOutputs(0.05, 0.007)
    }

    @Test
    fun test4() {
        testExpectedOutputs(2.5, 0.017)
    }

    @Test
    fun test5() {
        testExpectedOutputs(5.0, 0.039)
    }

    @Test
    fun test6() {
        testExpectedOutputs(15.5, 0.039)
    }

    private fun testExpectedOutputs(d: Double, expectedSac: Double) {
        val inputs = mapOf(
                PortsOfSteeringAngleCorrection.inPort.signedDistanceToTrajectory to d
        )
        val o = execute(inputs)
        val sac = o[PortsOfSteeringAngleCorrection.outPort.steeringAngleCorrection] as Double
        Assert.assertEquals(expectedSac, sac, 1e-3)
    }
}
