/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.MetaDataForNavigator
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.planning.PortsOfNavigator
import de.rwth.ryndin.modelling.structure.autopilot.Edge
import de.rwth.ryndin.modelling.structure.autopilot.Node
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.junit.Assert
import org.junit.Test

class NavigatorImplementationTest : ComponentTestBase(
        MetaDataForNavigator
) {

    private val nodes = listOf(
            Node(1L, Position(1.0, 1.0)),
            Node(2L, Position(2.0, 2.0)),
            Node(3L, Position(3.0, 3.0)),
            Node(4L, Position(4.0, 4.0)),
            Node(5L, Position(5.0, 5.0)),
            Node(6L, Position(6.0, 6.0)),
            Node(7L, Position(7.0, 7.0))
    )

    private val edges = listOf(
            Edge(1L, 2L, 10.0),
            Edge(2L, 3L, 20.0),
            Edge(3L, 4L, 30.0),
            Edge(4L, 5L, 40.0),
            Edge(5L, 1L, 50.0),
            Edge(6L, 7L, 60.0),
            Edge(7L, 6L, 70.0)
    )

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertNull(o[PortsOfNavigator.outPort.shortestPath])
    }

    @Test
    fun test2() {
        test(5L, 1L, listOf(Position(5.0, 5.0), Position(1.0, 1.0)))
    }

    @Test
    fun test3() {
        test(1L, 5L, listOf(
                Position(1.0, 1.0),
                Position(2.0, 2.0),
                Position(3.0, 3.0),
                Position(4.0, 4.0),
                Position(5.0, 5.0)
        ))
    }

    @Test
    fun test4() {
        test(1L, 5L, listOf(
                Position(1.0, 1.0),
                Position(2.0, 2.0),
                Position(3.0, 3.0),
                Position(4.0, 4.0),
                Position(5.0, 5.0)
        ))

        val o = execute(mapOf(
                PortsOfNavigator.inPort.goalNodeId to 3L,
                PortsOfNavigator.inPort.startNodeId to 1L
        ))
        Assert.assertEquals(
                listOf(
                        Position(1.0, 1.0),
                        Position(2.0, 2.0),
                        Position(3.0, 3.0)
                ),
                o[PortsOfNavigator.outPort.shortestPath]
        )
    }

    @Test
    fun test5() {
        test(1L, 5L, listOf(
                Position(1.0, 1.0),
                Position(2.0, 2.0),
                Position(3.0, 3.0),
                Position(4.0, 4.0),
                Position(5.0, 5.0)
        ))
        val o = execute(mapOf(
                PortsOfNavigator.inPort.startNodeId to 3L
        ))
        Assert.assertEquals(
                listOf(
                        Position(3.0, 3.0),
                        Position(4.0, 4.0),
                        Position(5.0, 5.0)
                ),
                o[PortsOfNavigator.outPort.shortestPath]
        )
    }

    @Test
    fun test6() {
        test(1L, 6L, listOf())
    }

    private fun test(startNodeId: Long, goalNodeId: Long, shortestPath: List<Position>) {
        val o = execute(mapOf(
                PortsOfNavigator.inPort.allEdges to edges,
                PortsOfNavigator.inPort.allNodes to nodes,
                PortsOfNavigator.inPort.goalNodeId to goalNodeId,
                PortsOfNavigator.inPort.startNodeId to startNodeId
        ))
        Assert.assertEquals(
                shortestPath,
                o[PortsOfNavigator.outPort.shortestPath]
        )
    }
}
