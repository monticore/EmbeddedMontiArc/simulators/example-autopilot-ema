/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.MetaDataForCalculateVelocity
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.PortsOfCalculateVelocity
import org.junit.Assert
import org.junit.Test

class CalculateVelocityImplementationTest : ComponentTestBase(
        MetaDataForCalculateVelocity
) {

    @Test
    fun test1() {
        testExpectedVelocity(0.0, 0.0)
    }

    @Test
    fun test2() {
        testExpectedVelocity(0.5, 0.0)
    }

    @Test
    fun test3() {
        testExpectedVelocity(1.0, 1.0)
    }

    @Test
    fun test4() {
        testExpectedVelocity(3.0, 1.732)
    }

    @Test
    fun test5() {
        testExpectedVelocity(5.0, 3.0)
    }

    @Test
    fun test6() {
        testExpectedVelocity(12.5, 3.0)
    }

    @Test
    fun test7() {
        testExpectedVelocity(20.0, 10.0)
    }

    @Test
    fun test8() {
        testExpectedVelocity(35.0, 11.401)
    }

    @Test
    fun test9() {
        testExpectedVelocity(50.0, 13.0)
    }

    @Test
    fun test10() {
        testExpectedVelocity(1000.0, 13.0)
    }

    private fun testExpectedVelocity(distanceToFinish: Double, expectedVelocity: Double) {
        val outputs = execute(mapOf(
                PortsOfCalculateVelocity.inPort.distanceToFinish to distanceToFinish
        ))
        val velocity = outputs[PortsOfCalculateVelocity.outPort.velocity] as Double
        Assert.assertEquals(expectedVelocity, velocity, 1e-2)
    }
}
