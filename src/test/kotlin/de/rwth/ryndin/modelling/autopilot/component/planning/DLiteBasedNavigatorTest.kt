/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.planning

import de.rwth.ryndin.modelling.structure.autopilot.Node
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.junit.Assert
import org.junit.Test

internal class DLiteBasedNavigatorTest {

    companion object {
        val TEST_NODES = mapOf(
                1L to Pair(0.0, 0.0)
                , 2L to Pair(0.0, 10.0)
                , 3L to Pair(7.0, 16.0)
                , 4L to Pair(21.0, 14.0)
                , 5L to Pair(25.0, 4.0)
                , 6L to Pair(20.0, -2.0)
                , 7L to Pair(18.0, 2.0)
                , 8L to Pair(10.0, -3.0)
                , 9L to Pair(10.0, 1.0)
                , 10L to Pair(5.0, 6.0)
                , 11L to Pair(7.0, 10.0)
                , 12L to Pair(13.0, 12.0)
                , 13L to Pair(19.0, 8.0)
                , 14L to Pair(11.0, 7.0)
        ).mapValues { Node(it.key, Position(it.value.first, it.value.second)) }

        val ALL_NODES_IDS = TEST_NODES.keys.toSet()

        val heuristic = EuclideanHeuristic(TEST_NODES)

        fun createEdge(from: Long, to: Long, k: Double): Pair<Pair<Id, Id>, Double> {
            val cost = k * heuristic(from, to)
            return Pair(from, to) to cost
        }

        fun createEdges(from: Long, to: Long, k: Double): List<Pair<Pair<Id, Id>, Double>> {
            return listOf(createEdge(from, to, k), createEdge(to, from, k))
        }

        val INITIAL_EDGES = createEdges(1, 2, 1.33)
                // outer ring
                .plus(createEdges(2, 3, 1.33))
                .plus(createEdges(3, 4, 1.33))
                .plus(createEdges(4, 5, 1.33))
                .plus(createEdges(5, 6, 1.33))
                .plus(createEdges(5, 7, 1.33))
                .plus(createEdges(6, 8, 1.33))
                .plus(createEdges(7, 8, 1.33))
                .plus(createEdges(8, 1, 1.33))
                // inner ring
                .plus(createEdge(7, 9, 3.7))
                .plus(createEdge(9, 10, 3.7))
                .plus(createEdge(10, 11, 3.7))
                .plus(createEdge(11, 12, 3.7))
                .plus(createEdge(12, 13, 3.7))
                .plus(createEdge(13, 7, 3.7))
                // outer ring <-> inner ring
                .plus(createEdge(9, 1, 1.33))
                .plus(createEdge(1, 10, 1.33))
                .plus(createEdge(3, 11, 1.33))
                .plus(createEdge(4, 12, 1.33))
                .plus(createEdge(13, 4, 1.33))
                // inner ring <-> center
                .plus(createEdge(7, 14, 7.3))
                .plus(createEdge(14, 9, 7.3))
                .plus(createEdge(10, 14, 7.3))
                .plus(createEdge(14, 12, 7.3))
                .plus(createEdge(14, 13, 7.3))
                .toMap()
    }

    lateinit var navigator: Navigator
    lateinit var referenceNavigator: Navigator

    @Test
    fun testRemoveEdges() {
        setup(1, 12)
        checkPathsForAllNodes()
        removeEdge(1, 10)
        checkPathsForAllNodes()
        removeEdge(7, 9)
        removeEdge(9, 10)
        removeEdge(10, 11)
        removeEdge(11, 12)
        removeEdge(12, 13)
        removeEdge(13, 7)
        checkPathsForAllNodes()
    }

    @Test
    fun testStartEqualsGoal() {
        setup(7, 7)
        checkPathsForAllNodes()
        removeEdge(7, 9)
        removeEdge(9, 10)
        removeEdge(10, 11)
        removeEdge(11, 12)
        removeEdge(12, 13)
        removeEdge(13, 7)
        checkPathsForAllNodes()
    }

    @Test
    fun testUpdateEdges() {
        setup(1, 1)
        checkPathsForAllNodes()
        addOrUpdateEdge(1, 10, 11 * heuristic(1, 10))
        addOrUpdateEdge(3, 11, 11 * heuristic(3, 11))
        checkPathsForAllNodes()
        addOrUpdateEdge(1, 3, 1.1 * heuristic(1, 3))
        addOrUpdateEdge(3, 1, 1.1 * heuristic(3, 1))
        addOrUpdateEdge(6, 9, 1.1 * heuristic(6, 9))
        checkPathsForAllNodes()
    }

    @Test
    fun testAllOperations() {
        setup(1, 14)
        checkPaths(1)
        removeEdge(1, 10)
        checkPaths(1)
        checkPaths(2)
        checkPaths(1)
        checkPaths(8)
        addOrUpdateEdge(8, 7, 100 * heuristic(8, 7))
        checkPaths(8)
        checkPaths(6)
        checkPaths(5)
        checkPaths(4)
        removeEdge(4, 5)
        removeEdge(5, 4)
        checkPaths(4)
    }

    @Test
    fun testNonReachableNode() {
        setup(1, 4)
        removeEdge(3, 4)
        removeEdge(4, 3)
        removeEdge(4, 5)
        removeEdge(5, 4)
        removeEdge(4, 12)
        removeEdge(13, 4)
        checkPathsForAllNodes()
    }

    @Test
    fun testFollowPath() {
        var nodeId = 1L
        val goalNodeId = 4L
        setup(nodeId, goalNodeId)
        while (nodeId != goalNodeId) {
            nodeId = getPathAndCheck(nodeId)[1]
        }
    }

    private fun addOrUpdateEdge(fromNodeId: Id, toNodeId: Id, cost: Double) {
        referenceNavigator.addOrUpdateEdge(fromNodeId, toNodeId, cost)
        navigator.addOrUpdateEdge(fromNodeId, toNodeId, cost)
    }

    private fun removeEdge(fromNodeId: Id, toNodeId: Id) {
        val expectedResult = referenceNavigator.removeEdge(fromNodeId, toNodeId)
        val result = navigator.removeEdge(fromNodeId, toNodeId)
        Assert.assertEquals(expectedResult, result)
    }

    private fun checkPathsForAllNodes() {
        ALL_NODES_IDS.forEach(this::checkPaths)
    }

    private fun checkPaths(fromNodeId: Id) {
        getPathAndCheck(fromNodeId)
    }

    private fun getPathAndCheck(fromNodeId: Id): List<Id> {
        val expectedPath = referenceNavigator.getShortestPathFrom(fromNodeId)
        val path = navigator.getShortestPathFrom(fromNodeId)
        Assert.assertEquals(expectedPath.size, path.size)
        expectedPath.forEachIndexed { index, nodeId ->
            Assert.assertEquals(nodeId, path[index])
        }
        return path
    }

    private fun setup(initialStartNode: Id, goalNodeId: Id) {
        navigator = DLiteBasedNavigator(INITIAL_EDGES, initialStartNode, goalNodeId, Companion.heuristic)
        referenceNavigator = DijkstraBasedNavigator(INITIAL_EDGES, goalNodeId)
    }
}
