/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter
import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.ConfigurationParametersOfDecideBehavior
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.MetaDataForDecideBehavior
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.PortsOfDecideBehavior
import de.rwth.ryndin.modelling.structure.autopilot.Actuation
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.SensorData
import de.rwth.ryndin.modelling.structure.autopilot.TurnMotionData
import de.rwth.ryndin.modelling.structure.autopilot.VehicleState
import org.junit.Assert
import org.junit.Test

class DecideBehaviorImplementationTest : ComponentTestBase(
        MetaDataForDecideBehavior,
        ComponentCreationParameters.create(listOf(), listOf(
                Parameter.create(ConfigurationParametersOfDecideBehavior.turnAngleThreshold, 5.0),
                Parameter.create(ConfigurationParametersOfDecideBehavior.unfeasibleTurnAngleThreshold, 120.0)
        ))
) {

    private val path = listOf(
            Position(0.0, 0.0),
            Position(0.0, 3.0),
            Position(0.0, 10.0),
            Position(10.0, 10.0),
            Position(0.0, 12.0)
    )

    @Test
    fun testDriveAlongDecision() {
        val cp = Position(0.0, 1.0)
        val vs = VehicleState(
                Actuation(1.5, 0.0, 0.0),
                SensorData(2.0, 0.0, cp)
        )
        val inputs = mapOf(
                PortsOfDecideBehavior.inPort.vehicleState to vs,
                PortsOfDecideBehavior.inPort.path to path,
                PortsOfDecideBehavior.inPort.isPathRebuilt to true
        )
        val o = execute(inputs)
        val driveAlongDecisionCurrentPosition = o[PortsOfDecideBehavior.outPort.driveAlongDecisionCurrentPosition] as? Position
        val driveAlongDecisionP1 = o[PortsOfDecideBehavior.outPort.driveAlongDecisionP1] as? Position
        val driveAlongDecisionP2 = o[PortsOfDecideBehavior.outPort.driveAlongDecisionP2] as? Position
        val driveAlongDecisionVelocity = o[PortsOfDecideBehavior.outPort.driveAlongDecisionVelocity] as? Double
        val driveToDecisionMinVelocity = o[PortsOfDecideBehavior.outPort.driveToDecisionMinVelocity] as? Double
        val driveToDecisionMaxVelocity = o[PortsOfDecideBehavior.outPort.driveToDecisionMaxVelocity] as? Double
        val driveToDecisionPosition = o[PortsOfDecideBehavior.outPort.driveToDecisionPosition] as? Position
        val driveTurnDecisionTurnMotionData = o[PortsOfDecideBehavior.outPort.driveTurnDecisionTurnMotionData] as? TurnMotionData
        val driveTurnDecisionVehicleState = o[PortsOfDecideBehavior.outPort.driveTurnDecisionVehicleState] as? VehicleState
        val driveToDecisionCurrentPosition = o[PortsOfDecideBehavior.outPort.driveToDecisionCurrentPosition] as? Position
        Assert.assertNotNull(driveAlongDecisionCurrentPosition)
        Assert.assertNotNull(driveAlongDecisionP1)
        Assert.assertNotNull(driveAlongDecisionP2)
        Assert.assertNotNull(driveAlongDecisionVelocity)
        Assert.assertNull(driveToDecisionMinVelocity)
        Assert.assertNull(driveToDecisionMaxVelocity)
        Assert.assertNull(driveToDecisionPosition)
        Assert.assertNull(driveToDecisionCurrentPosition)
        Assert.assertNull(driveTurnDecisionTurnMotionData)
        Assert.assertNull(driveTurnDecisionVehicleState)
        Assert.assertEquals(cp, driveAlongDecisionCurrentPosition)
        Assert.assertEquals(Position(0.0, 3.0), driveAlongDecisionP1)
        Assert.assertEquals(Position(0.0, 4.0), driveAlongDecisionP2)
        Assert.assertTrue(driveAlongDecisionVelocity!! > 0.0)
    }

    @Test
    fun testDriveToDecision() {
        val cp = Position(-1.0, 13.0)
        val vs = VehicleState(
                Actuation(1.5, 0.0, 0.0),
                SensorData(2.0, 0.0, cp)
        )
        val inputs = mapOf(
                PortsOfDecideBehavior.inPort.vehicleState to vs,
                PortsOfDecideBehavior.inPort.path to path,
                PortsOfDecideBehavior.inPort.isPathRebuilt to true
        )
        val o = execute(inputs)
        val driveAlongDecisionCurrentPosition = o[PortsOfDecideBehavior.outPort.driveAlongDecisionCurrentPosition] as? Position
        val driveAlongDecisionP1 = o[PortsOfDecideBehavior.outPort.driveAlongDecisionP1] as? Position
        val driveAlongDecisionP2 = o[PortsOfDecideBehavior.outPort.driveAlongDecisionP2] as? Position
        val driveAlongDecisionVelocity = o[PortsOfDecideBehavior.outPort.driveAlongDecisionVelocity] as? Double
        val driveToDecisionMinVelocity = o[PortsOfDecideBehavior.outPort.driveToDecisionMinVelocity] as? Double
        val driveToDecisionMaxVelocity = o[PortsOfDecideBehavior.outPort.driveToDecisionMaxVelocity] as? Double
        val driveToDecisionPosition = o[PortsOfDecideBehavior.outPort.driveToDecisionPosition] as? Position
        val driveTurnDecisionTurnMotionData = o[PortsOfDecideBehavior.outPort.driveTurnDecisionTurnMotionData] as? TurnMotionData
        val driveTurnDecisionVehicleState = o[PortsOfDecideBehavior.outPort.driveTurnDecisionVehicleState] as? VehicleState
        val driveToDecisionCurrentPosition = o[PortsOfDecideBehavior.outPort.driveToDecisionCurrentPosition] as? Position
        Assert.assertNull(driveAlongDecisionCurrentPosition)
        Assert.assertNull(driveAlongDecisionP1)
        Assert.assertNull(driveAlongDecisionP2)
        Assert.assertNull(driveAlongDecisionVelocity)
        Assert.assertNotNull(driveToDecisionMinVelocity)
        Assert.assertNotNull(driveToDecisionMaxVelocity)
        Assert.assertNotNull(driveToDecisionPosition)
        Assert.assertNotNull(driveToDecisionCurrentPosition)
        Assert.assertNull(driveTurnDecisionTurnMotionData)
        Assert.assertNull(driveTurnDecisionVehicleState)
        Assert.assertEquals(cp, driveToDecisionCurrentPosition)
        Assert.assertEquals(0.0, driveToDecisionMinVelocity!!, 1e-2)
        Assert.assertEquals(3.0, driveToDecisionMaxVelocity!!, 1e-2)
        Assert.assertEquals(Position(0.0, 12.0), driveToDecisionPosition!!)
    }

    @Test
    fun testDriveTurnDecision() {
        val cp = Position(0.0, 6.5)
        val vs = VehicleState(
                Actuation(1.5, 0.0, 0.0),
                SensorData(2.0, 0.0, cp)
        )
        val inputs = mapOf(
                PortsOfDecideBehavior.inPort.vehicleState to vs,
                PortsOfDecideBehavior.inPort.path to path,
                PortsOfDecideBehavior.inPort.isPathRebuilt to true
        )
        val o = execute(inputs)
        val driveAlongDecisionCurrentPosition = o[PortsOfDecideBehavior.outPort.driveAlongDecisionCurrentPosition] as? Position
        val driveAlongDecisionP1 = o[PortsOfDecideBehavior.outPort.driveAlongDecisionP1] as? Position
        val driveAlongDecisionP2 = o[PortsOfDecideBehavior.outPort.driveAlongDecisionP2] as? Position
        val driveAlongDecisionVelocity = o[PortsOfDecideBehavior.outPort.driveAlongDecisionVelocity] as? Double
        val driveToDecisionMinVelocity = o[PortsOfDecideBehavior.outPort.driveToDecisionMinVelocity] as? Double
        val driveToDecisionMaxVelocity = o[PortsOfDecideBehavior.outPort.driveToDecisionMaxVelocity] as? Double
        val driveToDecisionPosition = o[PortsOfDecideBehavior.outPort.driveToDecisionPosition] as? Position
        val driveTurnDecisionTurnMotionData = o[PortsOfDecideBehavior.outPort.driveTurnDecisionTurnMotionData] as? TurnMotionData
        val driveTurnDecisionVehicleState = o[PortsOfDecideBehavior.outPort.driveTurnDecisionVehicleState] as? VehicleState
        val driveToDecisionCurrentPosition = o[PortsOfDecideBehavior.outPort.driveToDecisionCurrentPosition] as? Position
        Assert.assertNull(driveAlongDecisionCurrentPosition)
        Assert.assertNull(driveAlongDecisionP1)
        Assert.assertNull(driveAlongDecisionP2)
        Assert.assertNull(driveAlongDecisionVelocity)
        Assert.assertNull(driveToDecisionMinVelocity)
        Assert.assertNull(driveToDecisionMaxVelocity)
        Assert.assertNull(driveToDecisionPosition)
        Assert.assertNull(driveToDecisionCurrentPosition)
        Assert.assertNotNull(driveTurnDecisionTurnMotionData)
        Assert.assertNotNull(driveTurnDecisionVehicleState)
        Assert.assertEquals(vs, driveTurnDecisionVehicleState)
    }

    @Test
    fun testEmptaPath() {
        val cp = Position(100.0, 100.0)
        val vs = VehicleState(
                Actuation(1.5, 0.0, 0.0),
                SensorData(2.0, 0.0, cp)
        )
        val inputs = mapOf(
                PortsOfDecideBehavior.inPort.vehicleState to vs
        )
        val o = execute(inputs)
        val driveAlongDecisionCurrentPosition = o[PortsOfDecideBehavior.outPort.driveAlongDecisionCurrentPosition] as? Position
        val driveAlongDecisionP1 = o[PortsOfDecideBehavior.outPort.driveAlongDecisionP1] as? Position
        val driveAlongDecisionP2 = o[PortsOfDecideBehavior.outPort.driveAlongDecisionP2] as? Position
        val driveAlongDecisionVelocity = o[PortsOfDecideBehavior.outPort.driveAlongDecisionVelocity] as? Double
        val driveToDecisionMinVelocity = o[PortsOfDecideBehavior.outPort.driveToDecisionMinVelocity] as? Double
        val driveToDecisionMaxVelocity = o[PortsOfDecideBehavior.outPort.driveToDecisionMaxVelocity] as? Double
        val driveToDecisionPosition = o[PortsOfDecideBehavior.outPort.driveToDecisionPosition] as? Position
        val driveTurnDecisionTurnMotionData = o[PortsOfDecideBehavior.outPort.driveTurnDecisionTurnMotionData] as? TurnMotionData
        val driveTurnDecisionVehicleState = o[PortsOfDecideBehavior.outPort.driveTurnDecisionVehicleState] as? VehicleState
        val driveToDecisionCurrentPosition = o[PortsOfDecideBehavior.outPort.driveToDecisionCurrentPosition] as? Position
        Assert.assertNull(driveAlongDecisionCurrentPosition)
        Assert.assertNull(driveAlongDecisionP1)
        Assert.assertNull(driveAlongDecisionP2)
        Assert.assertNull(driveAlongDecisionVelocity)
        Assert.assertNotNull(driveToDecisionMinVelocity)
        Assert.assertNotNull(driveToDecisionMaxVelocity)
        Assert.assertNotNull(driveToDecisionPosition)
        Assert.assertNotNull(driveToDecisionCurrentPosition)
        Assert.assertNull(driveTurnDecisionTurnMotionData)
        Assert.assertNull(driveTurnDecisionVehicleState)
        Assert.assertEquals(cp, driveToDecisionCurrentPosition)
        Assert.assertEquals(0.0, driveToDecisionMinVelocity!!, 1e-2)
        Assert.assertEquals(0.0, driveToDecisionMaxVelocity!!, 1e-2)
        Assert.assertEquals(cp, driveToDecisionPosition)
    }
}
