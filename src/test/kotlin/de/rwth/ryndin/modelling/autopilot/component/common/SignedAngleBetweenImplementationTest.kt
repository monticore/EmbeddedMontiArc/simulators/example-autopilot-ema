/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.MetaDataForSignedAngleBetween
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.PortsOfSignedAngleBetween
import de.rwth.ryndin.modelling.structure.autopilot.Vector
import org.junit.Assert
import org.junit.Test

class SignedAngleBetweenImplementationTest : ComponentTestBase(
        MetaDataForSignedAngleBetween
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertNull(o[PortsOfSignedAngleBetween.outPort.angle])
    }

    @Test
    fun test2() {
        testExpectedOutput(Vector(1.0, 0.0), Vector(0.0, -1.0), 1.57)
    }

    @Test
    fun test3() {
        testExpectedOutput(Vector(0.0, 1.0), Vector(-1.0, 0.0), -1.57)
    }

    @Test
    fun test4() {
        testExpectedOutput(Vector(1.0, 1.0), Vector(-1.0, 0.0), -2.35)
    }

    @Test
    fun test5() {
        testExpectedOutput(Vector(1.0, 1.0), Vector(10.0, 2.0), 0.58)
    }

    @Test
    fun test6() {
        testExpectedOutput(Vector(1.0, 1.0), Vector(-10.0, -10.0), -Math.PI)
    }

    @Test
    fun test7() {
        testExpectedOutput(Vector(0.0, 0.0), Vector(1.0, 1.0), 0.0)
    }

    @Test
    fun test8() {
        testExpectedOutput(Vector(1.0, 1.0), Vector(0.0, 0.0), 0.0)
    }

    private fun testExpectedOutput(v1: Vector, v2: Vector, expectedAngle: Double) {
        val o = execute(mapOf(
                PortsOfSignedAngleBetween.inPort.v1 to v1,
                PortsOfSignedAngleBetween.inPort.v2 to v2
        ))
        val actualAngle = o[PortsOfSignedAngleBetween.outPort.angle] as Double
        Assert.assertEquals(expectedAngle, actualAngle, 1e-2)
    }
}
