/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.motion

import de.monticore.lang.monticar.ema2kt.runtime.api.ComponentCreationParameters
import de.monticore.lang.monticar.ema2kt.runtime.api.Parameter
import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.ConfigurationParametersOfCalculatePidError
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.MetaDataForCalculatePidError
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.PortsOfCalculatePidError
import org.junit.Assert
import org.junit.Test

class CalculatePidErrorImplementationTest : ComponentTestBase(
        MetaDataForCalculatePidError,
        ComponentCreationParameters.create(listOf(), listOf(
                Parameter.create(ConfigurationParametersOfCalculatePidError.velocityDifferenceThresholdForErrorLeap, 1.5)
        ))
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        val error = o[PortsOfCalculatePidError.outPort.error] as Double
        Assert.assertEquals(0.0, error, 1e-2)
    }

    @Test
    fun test2() {
        testExpectedError(1.0, 1.0, 0.0)
    }

    @Test
    fun test3() {
        testExpectedError(1.0, 0.0, -100.0)
    }

    @Test
    fun test4() {
        testExpectedError(1.0, 5.0, 5.0)
    }

    @Test
    fun test5() {
        testExpectedError(10.0, 3.0, -10.0)
    }

    @Test
    fun test6() {
        testExpectedError(8.0, 9.1, 1.1)
    }

    @Test
    fun test7() {
        testExpectedError(10.0, 9.5, -0.5)
    }

    private fun testExpectedError(currentVelocity: Double, desiredVelocity: Double, expectedError: Double) {
        val inputs = mapOf(
                PortsOfCalculatePidError.inPort.currentVelocity to currentVelocity,
                PortsOfCalculatePidError.inPort.desiredVelocity to desiredVelocity
        )
        val o = execute(inputs)
        val error = o[PortsOfCalculatePidError.outPort.error] as Double
        Assert.assertEquals(expectedError, error, 1e-2)
    }
}
