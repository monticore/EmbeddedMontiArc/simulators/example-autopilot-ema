/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.behavior

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.MetaDataForApplyTurnWithFallbackToDriveAlongDirection
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.behavior.PortsOfApplyTurnWithFallbackToDriveAlongDirection
import de.rwth.ryndin.modelling.structure.autopilot.Actuation
import de.rwth.ryndin.modelling.structure.autopilot.Circle
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.SensorData
import de.rwth.ryndin.modelling.structure.autopilot.TurnMotionData
import de.rwth.ryndin.modelling.structure.autopilot.VehicleState
import org.junit.Assert
import org.junit.Test

class ApplyTurnWithFallbackToDriveAlongDirectionImplementationTest : ComponentTestBase(
        MetaDataForApplyTurnWithFallbackToDriveAlongDirection
) {

    @Test
    fun testFallbackLogic() {
        val currentPosition = Position(0.1, 4.0)
        val vehicleState = VehicleState(
                Actuation(1.5, 0.065, 0.0),
                SensorData(1.9, 0.314, currentPosition)
        )
        val start = Position(11.0, 0.0)
        val middle = Position(0.0, 0.0)
        val end = Position(0.0, 10.0)
        val turnMotionData = TurnMotionData(
                start,
                middle,
                end,
                2.0,
                0.7,
                Circle(Position(6.0, 6.0), 6.0)
        )
        val inputs = mapOf(
                PortsOfApplyTurnWithFallbackToDriveAlongDirection.inPort.vehicleState to vehicleState,
                PortsOfApplyTurnWithFallbackToDriveAlongDirection.inPort.turnMotionData to turnMotionData
        )
        val outputs = execute(inputs)
        Assert.assertEquals(6, outputs.size)
        val desiredSteeringAngle = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.desiredSteeringAngle]
        val desiredVelocity = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.desiredVelocity]
        val fallbackCurrentPosition = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackCurrentPosition]
        val fallbackP1 = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackP1]
        val fallbackP2 = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackP2]
        val fallbackVelocity = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackVelocity]
        Assert.assertNull(desiredSteeringAngle)
        Assert.assertNull(desiredVelocity)
        Assert.assertNotNull(fallbackCurrentPosition)
        Assert.assertNotNull(fallbackP1)
        Assert.assertNotNull(fallbackP2)
        Assert.assertNotNull(fallbackVelocity)
        Assert.assertEquals(currentPosition, fallbackCurrentPosition)
        Assert.assertEquals(middle, fallbackP1)
        Assert.assertEquals(end, fallbackP2)
        Assert.assertEquals(2.0, fallbackVelocity as Double, 1e-2)
    }

    @Test
    fun testTurnLogic1() {
        val currentPosition = Position(3.0, 0.5)
        val vehicleState = VehicleState(
                Actuation(1.5, 0.065, 0.0),
                SensorData(2.1, 1.5, currentPosition)
        )
        val start = Position(11.0, 0.0)
        val middle = Position(0.0, 0.0)
        val end = Position(0.0, 10.0)
        val turnMotionData = TurnMotionData(
                start,
                middle,
                end,
                2.0,
                0.7,
                Circle(Position(6.0, 6.0), 6.0)
        )
        val inputs = mapOf(
                PortsOfApplyTurnWithFallbackToDriveAlongDirection.inPort.vehicleState to vehicleState,
                PortsOfApplyTurnWithFallbackToDriveAlongDirection.inPort.turnMotionData to turnMotionData
        )
        val outputs = execute(inputs)
        Assert.assertEquals(6, outputs.size)
        val desiredSteeringAngle = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.desiredSteeringAngle]
        val desiredVelocity = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.desiredVelocity]
        val fallbackCurrentPosition = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackCurrentPosition]
        val fallbackP1 = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackP1]
        val fallbackP2 = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackP2]
        val fallbackVelocity = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackVelocity]
        Assert.assertNotNull(desiredSteeringAngle)
        Assert.assertNotNull(desiredVelocity)
        Assert.assertNull(fallbackCurrentPosition)
        Assert.assertNull(fallbackP1)
        Assert.assertNull(fallbackP2)
        Assert.assertNull(fallbackVelocity)
        Assert.assertEquals(0.745, desiredSteeringAngle as Double, 1e-3)
        Assert.assertEquals(2.0, desiredVelocity as Double, 1e-2)
    }

    @Test
    fun testTurnLogic2() {
        val currentPosition = Position(3.0, 0.804)
        val vehicleState = VehicleState(
                Actuation(1.5, 0.065, 0.0),
                SensorData(2.1, Math.PI / 2, currentPosition)
        )
        val start = Position(11.0, 0.0)
        val middle = Position(0.0, 0.0)
        val end = Position(0.0, 10.0)
        val turnMotionData = TurnMotionData(
                start,
                middle,
                end,
                2.0,
                0.7,
                Circle(Position(6.0, 6.0), 6.0)
        )
        val inputs = mapOf(
                PortsOfApplyTurnWithFallbackToDriveAlongDirection.inPort.vehicleState to vehicleState,
                PortsOfApplyTurnWithFallbackToDriveAlongDirection.inPort.turnMotionData to turnMotionData
        )
        val outputs = execute(inputs)
        Assert.assertEquals(6, outputs.size)
        val desiredSteeringAngle = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.desiredSteeringAngle]
        val desiredVelocity = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.desiredVelocity]
        val fallbackCurrentPosition = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackCurrentPosition]
        val fallbackP1 = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackP1]
        val fallbackP2 = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackP2]
        val fallbackVelocity = outputs[PortsOfApplyTurnWithFallbackToDriveAlongDirection.outPort.fallbackVelocity]
        Assert.assertNotNull(desiredSteeringAngle)
        Assert.assertNotNull(desiredVelocity)
        Assert.assertNull(fallbackCurrentPosition)
        Assert.assertNull(fallbackP1)
        Assert.assertNull(fallbackP2)
        Assert.assertNull(fallbackVelocity)
        Assert.assertEquals(0.7, desiredSteeringAngle as Double, 1e-3)
        Assert.assertEquals(2.0, desiredVelocity as Double, 1e-2)
    }
}
