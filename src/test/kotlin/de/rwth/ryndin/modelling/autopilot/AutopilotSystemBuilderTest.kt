/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot

import de.rwth.ryndin.modelling.meta.ToBeImplemented
import org.junit.Assert
import org.junit.Test

class AutopilotSystemBuilderTest {

    val target = AutopilotSystemBuilder()

    @Test
    fun allElementaryComponentsAreProvided() {
        ToBeImplemented.allIds.forEach {

            Assert.assertTrue(
                    "factory function for $it is not registered",
                    target.factory.isFactoryFunctionRegistered(it)
            )
        }
    }

    @Test
    fun autopilotComponentCanBeCreated() {
        Assert.assertNotNull(target.autopilotComponent)
    }
}
