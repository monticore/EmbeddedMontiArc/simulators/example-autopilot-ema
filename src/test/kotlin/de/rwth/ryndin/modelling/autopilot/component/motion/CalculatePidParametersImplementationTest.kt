/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.motion

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.MetaDataForCalculatePidParameters
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.motion.PortsOfCalculatePidParameters
import org.junit.Assert
import org.junit.Test

class CalculatePidParametersImplementationTest : ComponentTestBase(
        MetaDataForCalculatePidParameters
) {

    @Test
    fun test1() {
        val o = execute(mapOf())
        Assert.assertEquals(0.0, o[PortsOfCalculatePidParameters.outPort.paramP] as Double, 1e-2)
        Assert.assertEquals(0.0, o[PortsOfCalculatePidParameters.outPort.paramI] as Double, 1e-2)
        Assert.assertEquals(0.0, o[PortsOfCalculatePidParameters.outPort.paramD] as Double, 1e-2)
        Assert.assertEquals(0.0, o[PortsOfCalculatePidParameters.outPort.paramDecayCoefficient] as Double, 1e-2)
    }

    @Test
    fun test2() {
        testExpectedParameter(0.0, 0.0, 1.76)
    }

    @Test
    fun test3() {
        testExpectedParameter(1.0, 2.0, 1.82)
    }

    @Test
    fun test4() {
        testExpectedParameter(3.0, 7.0, 2.07)
    }

    @Test
    fun test5() {
        testExpectedParameter(13.0, 13.0, 3.29)
    }

    @Test
    fun test6() {
        testExpectedParameter(15.0, 13.0, 3.29)
    }

    private fun testExpectedParameter(currentVelocity: Double, desiredVelocity: Double, expectedP: Double) {
        val inputs = mapOf(
                PortsOfCalculatePidParameters.inPort.currentVelocity to currentVelocity,
                PortsOfCalculatePidParameters.inPort.desiredVelocity to desiredVelocity
        )
        val o = execute(inputs)
        val p = o[PortsOfCalculatePidParameters.outPort.paramP] as Double
        Assert.assertEquals(expectedP, p, 1e-2)
        Assert.assertEquals(0.0, o[PortsOfCalculatePidParameters.outPort.paramI] as Double, 1e-2)
        Assert.assertEquals(0.0, o[PortsOfCalculatePidParameters.outPort.paramD] as Double, 1e-2)
        Assert.assertEquals(0.0, o[PortsOfCalculatePidParameters.outPort.paramDecayCoefficient] as Double, 1e-2)
    }
}
