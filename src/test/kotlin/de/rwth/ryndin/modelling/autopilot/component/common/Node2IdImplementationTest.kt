/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.MetaDataForNode2Id
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.PortsOfNode2Id
import de.rwth.ryndin.modelling.structure.autopilot.Node
import de.rwth.ryndin.modelling.structure.autopilot.Position
import org.junit.Assert
import org.junit.Test

class Node2IdImplementationTest : ComponentTestBase(
        MetaDataForNode2Id
) {

    @Test
    fun test1() {
        val o = execute(mapOf(
                PortsOfNode2Id.inPort.node to Node(1L, Position(1.0, 1.0))
        ))
        Assert.assertEquals(1L, o[PortsOfNode2Id.outPort.id])
    }
}
