/* (c) https://github.com/MontiCore/monticore */
package de.rwth.ryndin.modelling.autopilot.component.common

import de.rwth.ryndin.modelling.autopilot.component.ComponentTestBase
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.MetaDataForVehicleState2CurrentPosition
import de.rwth.ryndin.modelling.meta.de.rwth.ryndin.modelling.component.autopilot.common.PortsOfVehicleState2CurrentPosition
import de.rwth.ryndin.modelling.structure.autopilot.Actuation
import de.rwth.ryndin.modelling.structure.autopilot.Position
import de.rwth.ryndin.modelling.structure.autopilot.SensorData
import de.rwth.ryndin.modelling.structure.autopilot.VehicleState
import org.junit.Assert
import org.junit.Test

class VehicleState2CurrentPositionImplementationTest : ComponentTestBase(
        MetaDataForVehicleState2CurrentPosition
) {

    @Test
    fun test1() {
        val cp = Position(1.0, 1.0)
        val vehicleState = VehicleState(
                Actuation(0.0, 0.0, 0.0),
                SensorData(0.0, 0.0, cp)
        )
        val o = execute(mapOf(
                PortsOfVehicleState2CurrentPosition.inPort.vehicleState to vehicleState
        ))
        Assert.assertEquals(cp, o[PortsOfVehicleState2CurrentPosition.outPort.currentPosition])
    }
}
