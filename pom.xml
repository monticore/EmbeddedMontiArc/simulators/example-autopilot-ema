<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) https://github.com/MontiCore/monticore -->
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>montisim</groupId>
    <artifactId>example-autopilot-ema</artifactId>
    <version>0.0.5-SNAPSHOT</version>

    <properties>
        <kotlin.version>1.1.51</kotlin.version>
        <ema2kt.version>0.0.5</ema2kt.version>
        <junit.version>4.12</junit.version>
        <jackson.version>2.9.1</jackson.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib-jre8</artifactId>
            <version>${kotlin.version}</version>
        </dependency>
        <dependency>
            <groupId>de.monticore.lang.monticar.ema2kt</groupId>
            <artifactId>runtime</artifactId>
            <version>${ema2kt.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>de.monticore.lang.monticar.ema2kt</groupId>
                <artifactId>ema2kt-maven-plugin</artifactId>
                <version>${ema2kt.version}</version>
                <executions>
                    <execution>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <packagePrefix>de.rwth.ryndin.modelling</packagePrefix>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>kotlin-maven-plugin</artifactId>
                <groupId>org.jetbrains.kotlin</groupId>
                <version>${kotlin.version}</version>
                <executions>
                    <execution>
                        <id>compile</id>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                        <configuration>
                            <sourceDirs>
                                <sourceDir>${project.basedir}/target/src-gen/kotlin</sourceDir>
                                <sourceDir>${project.basedir}/src/main/kotlin</sourceDir>
                            </sourceDirs>
                        </configuration>
                    </execution>
                    <execution>
                        <id>test-compile</id>
                        <goals>
                            <goal>test-compile</goal>
                        </goals>
                        <configuration>
                            <sourceDirs>
                                <sourceDir>${project.basedir}/src/test/kotlin</sourceDir>
                            </sourceDirs>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>com.mycila</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <version>3.0</version>
                <configuration>
                    <header>${project.baseUri}/src/license/se/license.txt</header>
                    <properties>
                        <owner>Michael von Wenckstern</owner>
                        <email>vonwenckstern@se-rwth.de</email>
                    </properties>
                    <excludes>
                        <exclude>**/README</exclude>
                        <exclude>src/test/resources/**</exclude>
                        <exclude>src/main/resources/**</exclude>
                        <exclude>src/license/**</exclude>
                    </excludes>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>format</goal>
                        </goals>
                        <phase>process-sources</phase>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.19.1</version>
                <configuration>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.eluder.coveralls</groupId>
                <artifactId>coveralls-maven-plugin</artifactId>
                <version>4.3.0</version>
                <configuration>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>cobertura-maven-plugin</artifactId>
                <version>2.7</version>
                <configuration>
                    <format>xml</format>
                    <maxmem>256m</maxmem>
                    <!-- aggregated reports for multi-module projects -->
                    <aggregate>true</aggregate>
                </configuration>
			</plugin>

            
        </plugins>
    </build>

 <!-- == DISTRIBUTION ==================================================== -->

  <distributionManagement>
    <repository>
      <id>se-nexus</id>
      <url>https://nexus.se.rwth-aachen.de/content/repositories/embeddedmontiarc-releases/</url>
    </repository>
    <snapshotRepository>
      <id>se-nexus</id>
      <url>https://nexus.se.rwth-aachen.de/content/repositories/embeddedmontiarc-snapshots/</url>
    </snapshotRepository>
  </distributionManagement>


</project>
