<!-- (c) https://github.com/MontiCore/monticore -->
[![Build Status](https://travis-ci.org/MontiSim/example-autopilot-ema.svg?branch=master)](https://travis-ci.org/MontiSim/example-autopilot-ema)

# Autopilot.ema
Example of an Autopilot.ema component built by EMA2Kotlin maven plugin
